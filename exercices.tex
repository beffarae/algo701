\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{Algo 701 -- quelques exercices}
\author{Emmanuel Beffara}
\date{M1 MEEF maths 2020-2021, UGA}

\usepackage         {amsmath}
\usepackage[french] {babel}
\usepackage         {enumitem}
\usepackage[a4paper]{geometry}
\usepackage         {multicol}
\usepackage         {scratch3}
\usepackage         {tabularx}
\usepackage[small]  {titlesec}
\usepackage         {url}

\tikzset{line/.style={color=blue,thick},x=4mm,y=4mm}
\setscratch{scale=0.7}
\def\og{``}\def\fg{''}

\begin{document}
\maketitle

\section{Un peu de Scratch}

L'exercice suivant est adapté d'un sujet donné au brevet des collèges en série
générale en septembre 2017.

\medskip

La figure \ref{fig:dessins} contient trois figures différentes, aucune n'est à
l'échelle indiquée dans l'exercice.
La figure \ref{fig:programme} contient un programme Scratch qui utilise une
variable nommée \og\emph{longueur}\fg.
On rappelle que l'instruction \og s’orienter à 90\fg\ signifie que
l'on s'oriente vers la droite avec le stylo.

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \draw [line] (0,0)--(0,4.3)--(3.8,4.3)--(3.8,1)--(1,1)--(1,3.3)--(2.7,3.3)--(2.7,2.2)--(2.2,2.2);
    \node at (2.2,-1) {dessin 1};
  \end{tikzpicture}
  \hfil
  \begin{tikzpicture}
    \draw [line] (4.9,2.3)--(3.6,0)--(0.9,0)--(0,1.7)--(1,3.4)--(2.4,3.4)--(3,2.3)--(2.7,1.7)--(2,1.7);
    \node at (2,-1.5) {dessin 2};
  \end{tikzpicture}
  \hfil
  \begin{tikzpicture}
    \draw [line] (0.5,3.4)--(3.4,3.4)--(3.4,1.4)--(1.2,1.4)--(1.2,2.7)--(2.7,2.7)--(2.7,1.9)--(1.9,1.9);
    \node at (2,0.5) {dessin 3};
  \end{tikzpicture}
  \caption{Trois dessins réalisables en Scratch}
  \label{fig:dessins}
\end{figure}

\begin{figure}[ht]
  \centering
  \begin{scratch}
    \blockinit{Quand \greenflag est cliqué}
    \blocklook{cacher}
    \blockmove{aller à x: \ovalnum{0} y: \ovalnum{0}}
    \blockmove{s’orienter à \ovalnum{90}}
    \blockvariable{mettre \selectmenu{longueur} à   \ovalnum{30} }
    \blockpen{effacer tout}
    \blockpen{mettre la taille du stylo à \ovalnum{3}}
    \blockpen{stylo en position d'écriture}
    \blockrepeat{répéter \ovalnum{2} fois}
    { \blockmoreblocks{un tour}
    \blockvariable{ajouter  à \selectmenu{longueur} \ovalnum{30} } }
  \end{scratch}
  \hfil
  \begin{scratch}
    \initmoreblocks{Définir \namemoreblocks{un tour}}
    \blockrepeat{répéter \ovalnum{2} fois}
    { \blockmove{avancer de \ovalvariable{longueur} }
    \blockmove{tourner \turnleft{} de \ovalnum{90} degrés} }
    \blockvariable{ajouter à  \selectmenu{longueur}  \ovalnum{30} }
    \blockrepeat{répéter \ovalnum{2} fois}
    { \blockmove{avancer de \ovalvariable{longueur} }
    \blockmove{tourner \turnleft{} de \ovalnum{90} degrés} }
  \end{scratch}
  \caption{Un programme en Scratch}
  \label{fig:programme}
\end{figure}

\begin{enumerate}
  \item
    \begin{enumerate}
      \item Dessiner la figure obtenue avec le bloc «~un tour~» donné dans la
        figure~\ref{fig:programme}, pour une longueur de départ égale à 30,
        étant orienté vers la droite avec le stylo, en début de tracé. On
        précisera l'échelle employer pour faire le dessin.
      \item Comment est-on orienté avec le stylo après ce tracé?
    \end{enumerate}
  \item Lequel des dessins 1 ou 3 le programme de la
    figure~\ref{fig:programme} permet-il d'obtenir? Justifier.
  \item Quelle modification faut-il apporter au bloc «~\emph{un tour}~» pour
    obtenir le dessin 2 de la figure~\ref{fig:dessins}?
\end{enumerate}


\section{Méthode de la fausse position}

La \emph{méthode de la fausse position} peut être vue comme une amélioratoin
de la méthode de dichotomie pour rechercher un zéro d'une fonction continue.
Elle consiste à couper en deux l'intervalle de recherche non pas en son milieu
mais là où se trouverait le zéro si la fonction étudiée était affine. La
figure~\ref{fig:position} illustre graphiquement l'étape de calcul
fondamentale.

\begin{figure}
  \shorthandoff{;}
  \centering
  \begin{tikzpicture}[x=1cm,y=1cm,
      declare function={
        f(\a)=\a*\a*\a/13-\a*\a/3-\a/3+0.5; }]
    \draw[-latex] (-1,0) -- (6,0);
    \draw[-latex] (0,-1) -- (0,2);
    \draw[thick,domain=-1:5.5,samples=50] plot (\x,{f(\x)});
    \begin{scope}[color=red!60!black]
      \def\a{3} \def\b{5.2}
      \fill (\a,{f(\a)}) circle (2pt) node[below] {$a$};
      \fill (\b,{f(\b)}) circle (2pt) node[right] {$b$};
      \draw[thick] (\a,{f(\a)}) -- (\b,{f(\b)});
      \fill ({\a+(\b-\a)*f(\a)/(f(\a)-f(\b))},0) circle (2pt) node[above left] {$c$};
    \end{scope}
  \end{tikzpicture}
  \caption{Principe de la méthode de la fausse position}
  \label{fig:position}
\end{figure}

\begin{enumerate}
  \item Écrire proprement l'algorithme de recherche de zéro d'une fonction $f$
    à une précision $\epsilon$ près par la méthode de la fausse position,
    entre deux points $a$ et $b$ donnés, en supposant $f(a)\leq0$ et
    $f(b)\geq0$.
  \item Montrer que le nombre d'itérations de cette méthode est au plus le
    nombre d'itérations de la méthode de dichotomie.
  \item Justifier pourquoi cette méthode est plus efficace que la dichotomie
    dans le cas d'une fonction de classe $C^2$.
\end{enumerate}

\section{Recherche d'élément majoritaire}

Étant donnée une liste $L$ de $n$ éléments (qui peuvent se répéter), le
problème de l'élément majoritaire consiste à déterminer s'il existe un élément
dont le nombre d'occurences est plus grand que $n/2$. On ne précise pas la
nature des éléments, on peut juste savoir s'ils ont la mêmevaleur ou pas. Par
exemple, on a un sac contenant des balles de différentes couleurs et on
cherche à déterminer si au moins la moitié des balles ont la même couleur.

Les trois parties suivantes sont indépendantes, elles se consacrent à des
algorithmes différents pour résoudre ce problème.

\begin{enumerate}
  \item On commence par étudier un algorithme \og naïf\fg\ pour résoudre ce
    problème.
    \begin{enumerate}
      \item Décrire un algorithme qui recense les éléments distincts et qui
        calcule pour chacun d'eux son nombre d'occurences.
      \item Quelle est la complexité de cet algorithme ?
    \end{enumerate}
  \item On s'intéresse maintenant à une approche récursive de type \og diviser
    pour régner\fg.
    \begin{enumerate}
      \item On divise la liste $L$ en deux sous-listes $L_1$ et $L_2$ de même
        taille (à une unité près). Montrer qu'un élément qui n'est majoritaire
        ni dans $L_1$ ni dans $L_2$ ne peut pas être majoritaire dans $L$.
      \item En déduire un algorithme récursif pour le calcul d'un élément
        majoritaire.
      \item Montrer que la complexité de cet algorithme est $O(n\ log\ n)$.
    \end{enumerate}
  \item On va maintenant montrer que l'algorithme suivant permet de résoudre
    le problème avec une complexité optimale.

    Reprenons l'image du sac contenant des balles de différentes couleurs. On
    dispose d'une étagère pour poser les balles les unes à côté des autres et
    d'une corbeille pouvant contenir autant de balles que nécessaire.
    On donne l'algorithme suivant:
    \begin{tabbing}
      \qquad\=\qquad\=\qquad\=\qquad\=\kill
      \emph{Phase 1.} \\
      \>Tant que le sac n'est pas vide:\\
      \>\> Prendre une balle $b$ dans le sac. \\
      \>\> Si la dernière balle posée sur l'étagère est de la même couleur que $b$, \\
      \>\>\> placer $b$ dans la corbeille. \\
      \>\> Sinon, \\
      \>\>\> placer $b$ sur l'étagère; \\
      \>\>\> si la corbeille n'est pas vide, \\
      \>\>\>\> prendre une balle dans la corbeille et la placer à côté de $b$ sur l'étagère. \\
      \emph{Phase 2.} \\
      \> Soit $c$ la couleur de la dernière balle posée sur l'étagère. \\
      \> Calculer le nombre $m$ de balles de couleur $c$ en tout. \\
      \> Si $m>n/2$ alors $c$ est majoritaire sinon il n'y a aucun élément majoritaire.
    \end{tabbing}
    \begin{enumerate}
      \item Montrer par récurrence sur le nombre d'itérations de la phase 1,
        qu'à la fin de chacune des itérations, toutes les balles de la
        corbeille sont de la même couleur que la dernière balle posée sur
        l'étagère.
      \item Justifier que deux balles placées consécutivement sur l'étagère
        ne peuvent pas avoir la même couleur.
      \item En déduire que seule la couleur de la dernière balle posée sur
        l'étagère peut être majoritaire.
      \item Analyser la complexité de cet algorithme.
    \end{enumerate}
\end{enumerate}

\end{document}
