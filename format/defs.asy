/* Iterate a set of transformations on an array of paths. */

path[] iter(transform[] T, path[] p, int n=1) {
  for (int i = 0; i < n; ++i) {
    path[] r = {};
    for (int i = 0; i < T.length; ++i) r = r ^^ T[i] * p;
    p = r;
  }
  return p;
}

/* The transformation set for Koch. */

transform[] kochT = {
   scale(1/3),
   shift(1/3,0)*scale(1/3)*rotate(60),
   shift(1/2,sqrt(3)/6)*scale(1/3)*rotate(-60),
   shift(2/3,0)*scale(1/3)
};

/* The transformation set for the plant. */

transform[] plantT = {
   scale(1/2),
   shift(1/2,0) * rotate(30) * scale(1/2),
   shift(1/2,0) * scale(1/2),
   shift(1,0) * rotate(-30) * scale(1/2)
};
