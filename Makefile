JUPYTER = \
      Kaprekar \
      Euclide Euclide-rempli \
      Zeros Zeros-rempli \
      TP-note
WEB = \
      index.html \
      diapos-aleatoire.pdf \
      diapos-intro.pdf \
      diapos-racine.pdf \
      diapos-scratch.pdf \
      diapos-zeros.pdf \
      tp-aleatoire.pdf tp-aleatoire.html tp-aleatoire.ipynb \
      tp-dessin.pdf tp-dessin.html \
      tp-enveloppes.pdf tp-enveloppes.html tp-enveloppes.ipynb \
      tp-evenementiel.pdf tp-evenementiel.html tp-evenementiel-notes.html \
      tp-majoritaire.pdf tp-majoritaire.html \
      tp-monnaie.pdf tp-monnaie.html tp-monnaie-correction.pdf tp-monnaie-correction.html \
      tp-racine.pdf tp-racine.html tp-racine.ipynb \
      tp-scratch.pdf tp-scratch.html \
      tp-sous-liste.pdf tp-sous-liste.html \
      examen.pdf \
      $(addsuffix .ipynb,$(JUPYTER)) $(addsuffix .html,$(JUPYTER))

.PHONY: web

web: $(addprefix public/,$(WEB))

public/diapos-%.pdf: diapos/%.md format/diapos.yaml
	# pandoc -t beamer --pdf-engine xelatex $^ -o $@
	pandoc -t beamer --pdf-engine xelatex --filter pandoc-imagine $^ -o $@
public/tp-%.pdf: tp/%.md format/tp-pdf.yaml
	pandoc -t latex --filter pandoc-imagine $^ -o $@
public/tp-%.html: tp/%.md format/tp-html.yaml
	pandoc --standalone -c style.css -t html --filter pandoc-imagine $^ -o $@
	for F in $$(sed -n 's;.* src="\(pd-images/[^"]*\)".*;\1;p' $@); do install -D $$F public/$$F; done
public/%.ipynb: %.md
	pandoc $^ -o $@
public/%.ipynb: jupyter/%.ipynb
	cp -f $^ $@
public/%.html: jupyter/%.ipynb
	jupyter nbconvert --to html --stdout $< > $@
public/index.html: README.md
	pandoc -t html --template=format/template.html --metadata pagetitle=Algo701 $< -o $@ 
