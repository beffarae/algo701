% Calcul de racines carrées
% Emmanuel Beffara
% M1 MEEF maths 2023-2024, UGA

## Le problème

**Objectif:** Calculer la racine carrée d'un nombre.

. . .

Bien entendu, il s'agit de calculer une *approximation*.

. . .

- On étudie des algorithmes pour calculer des approximations à une précision
  $\epsilon$ près.
- On formule ces algorithmes dans les nombres réels.
- Les réels n'existent pas en programmation : on travaille à précision
  limitée.

# Recherche linéaire

## Recherche par balayage

L'idée est de tester des valeurs successives avec un pas donné:

| $x$ | $x^2$ |
|-----|-------|
| 0.8 |  0.64 |
| 0.9 |  0.81 |
| 1.0 |  1.00 |
| 1.1 |  1.21 |
| 1.2 |  1.44 |
| 1.3 |  1.69 |
| 1.4 |  1.96 |
| 1.5 |  2.25 |
| 1.6 |  2.56 |
| 1.7 |  2.89 |

En diminuant le pas, on obtient une meilleure précision.

## Analyse de la recherche par balayage

### Exercice

Formuler précisément l'algorithme de recherche linéaire et démontrer sa
correction.

- Spécifier les entrées et la sortie.

- Préciser les variables utilisées et les invariants.

### TP pour plus tard

Programmer un calcul de racine carrée par balayage (une seule étape).

# Dichotomie

## Recherche dichotomique

On part d'une fonction $f:\mathbb{R}\to\mathbb{R}$ supposée continue et de
deux réels $a$ et $b$ tels que $a<b$ et $f(a)\leq0$ et $f(b)\geq0$.

### Algorithme de recherche dichotomique

Tant que $b-a>\epsilon$:\
    $c \leftarrow (a+b)/2$\
    Si $f(c) \geq 0$, alors\
        $b \leftarrow c$\
    sinon\
        $a \leftarrow c$\
Renvoyer $a$

## Analyse de la recherche dichotomique

### Exercice

- Démontrer que l'algorithme est correct si on le considère sur les nombres
  réels.

- Déterminer le nombre d'itérations.

### Discussion

- Qu'est-ce qui change quand on applique cet algorithme à des nombres de
  précision limitée?

- Quel rapport avec la recherche dichotomique dans un tableau?

# Méthode de Héron

## Algorithme de Héron

On définit une suite récurrente:

$$
  \begin{aligned}
    x_0 &= 1 \\
    x_{n+1} &= \dfrac{x_n + \dfrac{a}{x_n}}{2} &&\text{pour tout $n$}
  \end{aligned}
$$

On démontre que cette suite converge vers $\sqrt{a}$.

## Explication géométrique

``` asy
import graph;
unitsize(1.5cm);
pen axes = rgb(.8,.8,.8);
draw((-1,0) -- (7,0), arrow=Arrow, p=axes);
draw((0,-0.3) -- (0,4.5), arrow=Arrow, p=axes);
real f(real x) { return 2/x; }
pen curve = linewidth(1pt) + rgb(1,1,1);
draw(graph(f, 0.48, 6.5), p=curve);
label("$a/x$", (6.5, f(6.5)), up, p=curve);
draw((0,0) -- (3.5,3.5), p=curve);
label("$x$", (3.5, 3.5), right, p=curve);
real x = 4;
for (int i = 0; i < 4; ++i) {
    real y = f(x);
    draw((y,0) -- (y,x) -- (0,x), p=rgb(0.7,0.7,0.7));
    draw((x,0) -- (x,y) -- (0,y), p=rgb(1,.3,.3));
    draw((x,y), p=linewidth(3pt)+rgb(1,.3,.3));
    draw((x,0), p=linewidth(3pt)+rgb(1,.3,.3));
    label("$x_{" + string(i) + "}$", (x + (i == 3 ? -0.1 : 0.1), 0), down, p=axes);
    x = (x + y) / 2;
}
```

## Analyse de l'algorithme

### Exercice

- Démontrer que la suite converge vers $\sqrt{a}$.
- Estimer le nombre d'itérations pour approcher la limite à $\epsilon$ près,
  en fonction de $a$ et $\epsilon$.

### Exercice

Donner un algorithme pour calculer un approximation de $\sqrt[n]a$ à
$\epsilon$ près en fonction de $n$, $a$ et $\epsilon$.

# Calculs en précision limitée

## TP

Si on implémente l'algorithme, on ne peut pas calculer avec des nombres réels.

### Exercice

Donner un algorithme permettant de calculer $\lfloor\sqrt{n}\rfloor$ pour un
entier $n$ et donner sa complexité.

### Exercice

Donner un algorithme permettant de calculer la meilleure approximation
possible de $\sqrt{x}$ pour un nombre $x$ représenté avec $m$ bits
significatifs.

### Exercice

Programmer tout ça.
