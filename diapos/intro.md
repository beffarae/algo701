% Algorithmique et programmation
% Emmanuel Beffara
% M1 MEEF maths 2023-2024, UGA

# Introduction

## Objectifs

Permettre aux futurs professeurs d’avoir une démarche algorithmique sur des
notions mathématiques enseignées au lycée et au collège. Travail sur la
programmation et la maîtrise des outils informatiques. Une part importante
de cet enseignement est effectuée sous forme de TP sur machines.

## Organisation

Les séances:

- certaines sans machines
- certaines avec du Scratch
- certaines avec de la programmation en Python

Évaluation prévue:

- un rendu écrit en cours de semestre
- un TP en Python noté
- un contrôle sur table en fin de semestre

# Vue d'ensemble

## L'instrumentation en mathématiques

L'emploi d'instruments est aussi vieux que les mathématiques:

- bouliers et autres abaques,
- règles et compas,
- variétés d'un instruments de mesure…

L'informatique est née du besoin d'instrumentation:

- calculs numériques,
- simulation,
- visualisation,
- calcul formel…

## L'outil informatique dans l'enseignement

Les instruments font partie de la pratique mathématique, ils ont donc
naturellement leur place dans l'enseignement.

Utilisation de calculatrices et de logiciels:

- calculs numériques
- tracé de courbes
- géométrie dynamique
- calcul formel

Les usages possibles sont nombreux.

Comment s'en servir de façon pertinente?

## Automatismes et automatisation

Les étapes de l'instrumentation du calcul:

1. découverte de techniques de calcul,
2. systématisation de la technique,
3. développement d'automatismes et d'intuitions associées,
4. transfert de la tâche automatique vers la machine.

Ce processus nécessite de comprendre les objets et leur manipulation avant
de s'économiser les calculs faits à la main.

- Cela s'applique bien dans le cas du calcul *exact*, dans le domaine du
  *discret*.
- Dans le domaine du *continu*, cela ouvre des questions importantes:
  précision, approximation…

## Les objets mathématiques vus de l'informatique

Les nombres:

- en cours de mathématiques: entiers, décimaux, rationnels, réels, complexes
- en informatique: la notion de précision est incontournable quand on parle
  des nombres non entiers
- comment les différentes notions de *nombre* sont-elle comprises?

Les fonctions:

- en mathématiques savantes: une relation
- en informatique: un procédé de calcul
- et dans l'esprit d'un élève?

La géométrie, mesure de l'espace:

- la carte: constructions, approche statique et globale
- la navigation: déplacements, approche dynamique et locale


# La pensée informatique

## L'informatique est aussi une science

Quand on emploie des outils informatiques, on ne peut pas faire l'économie
d'une étude de la démarche informatique elle-même.

Les quatre piliers de l'informatique:

- machine
- information
- algorithme
- langage

Toute science comporte son lot d'abstractions.

## Représentation de l'information

L'informatique ne traite pas les objets mathématiques mais leur représentation
symbolique.

- Il est nécessaire de distinguer l'objet de son écriture.
- Le nombre n'est pas son écriture en chiffres.

En dehors du discret, la représentation induit une restriction de l'espace des
objets:

- nombres décimaux et leurs analogues
- fonctions calculables

Quel rapport entre l'objet mathématique et l'objet informatique?

## Algorithme et programme

Un algorithme est une méthode systématique et non ambiguë permettant de
résoudre en un temps fini toutes les instances d'un problème calculatoire
formellement posé:

- concerne des objets mathématiques abstraits,
- suppose une *généricité*,
- est écrit, lu et étudié par des humains.

Un programme est la mise en œuvre d'un ou plusieurs algorithmes dans un
langage de programmation particulier:

- suppose de préciser des aspects concrets spécifiques au langage et
  à l'environnement utilisés,
- est fait pour être lu par des humains et interprété par une machine.

Un algorithme n'a pas forcément vocation à être programmé !


## Enseigner les abstractions informatiques

Avec la machine:

- micro-mondes pour la programmation
- utiliser les outils pour comprendre les représentations

Sans la machine:

- informatique débranchée: activités manuelles mettant en jeu les éléments
  fondamentaux de la pensée informatique
- mathématiques discrètes (arithmétique, combinatoire, graphes…)

Quelle interaction avec les mathématiques?

- donner un sens opératoire aux raisonnements
- proposer des cadres d'expérimentation pour les notions abstraites
- se confronter aux questions liées spécifiquement à l'écriture des objets


## L'informatique en cours de mathématiques

Le but (pour moi aujourd'hui, pour vous demain) n'est pas de faire un cours
complet d'informatique mais d'apporter les éléments pertinents pour
l'enseignement des mathématiques.

- centré sur l'algorithmique
- construit autour de notions pertinentes pour le programme de mathématiques
- met en avant la démarche expérimentale: rétroaction rapide, dédramatisation
  de l'erreur, intransigeance de la machine

Différents contextes:

- mathématiques de cycle 4 : éléments de pensée algorithmique, manipulation
  avec Scratch (déplacement de lutins, interaction)
- aspects algo en SNT en 2nde (programmation Python, notion de fonction)
- thèmes d'algo et programmation des mathématiques de lycée (notion de liste)

C'est un ensemble de notions transversales, au même titre que le vocabulaire
ensembliste et la logique.

# En route

## La recette de Kaprekar

Suivre cette procédure avec plusieurs nombres de départ différents:

1. Choisir un nombre à 4 chiffres.
2. Écrire le plus grand nombre possible avec ces chiffres, ainsi que le plus
   petit possible.
3. Calculer la différence entre ces deux nombres.
4. Recommencer à partir de l’étape 2 avec le résultat obtenu.

Quelques questions:

- Que remarque-t-on ?
- Comment le vérifier ? (par le calcul, pas par l'algèbre)

## Recette de Kaprekar: élaboration de l'algorithme

1. De quelles variables aurons-nous besoin ?
2. Rédiger les traitements partiels (par groupes)
3. Rédiger le traitement complet



