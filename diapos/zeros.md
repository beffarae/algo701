% Recherche de zéros d'une fonction
% Emmanuel Beffara
% M1 MEEF maths 2021-2022, UGA

# Le problème

## Problème

Calculer un zéro d'une fonction $f:\mathbb{R}\to\mathbb{R}$.

\bigskip

. . .

Quelques points à préciser:

- On doit supposer que la fonction $f$ a des propriétés de régularité:
  continue, dérivable…

- On formule le problème dans les nombres réels mais on veut programmer le
  calcul, ce qui utilisera des valeurs approchées.

# Recherche dichotomique, graphiquement

``` asy
import graph;
unitsize(1.2cm);
draw((-1,0) -- (7,0), arrow=Arrow);
draw((0,-1.5) -- (0,4), arrow=Arrow);
real f(real x) { return (x*x*x+x)/30-1; }
pen curve = linewidth(1pt);
pen marker = rgb(0.6,0,0);
pen dot = linewidth(4pt) + marker;
draw(graph(f, -1, 5.1), p=curve);
real mark(real x)
{
    real fx = f(x);
    draw((x, f(x)), p=dot);
    draw((x,0) -- (x,f(x)), p=marker);
    return fx;
}
real a = 1, b = 5;
mark(a); mark(b);
for (int i = 0; i < 5; ++i) {
    real c = (a + b) / 2;
    real fc = mark(c);
    if (fc >= 0)
        b = c;
    else
        a = c;
}
```

# Recherche dichotomique

On part d'une fonction $f:\mathbb{R}\to\mathbb{R}$ supposée continue et de deux réels $a$ et $b$
tels que $f(a)\leq0$ et $f(b)\geq0$.

## Algorithme de recherche dichotomique

Tant que $b-a>\epsilon$:  
    $c \leftarrow (a+b)/2$  
    Si $f(c) \geq 0$, alors  
        $b \leftarrow c$  
    sinon  
        $a \leftarrow c$  
Renvoyer $a$

## Correction, analyse, propriétés…

On en a déjà discuté la semaine dernière.

# Méthode de Newton, graphiquement

``` asy
import graph;
unitsize(1.2cm);
draw((-1,0) -- (7,0), arrow=Arrow);
draw((0,-1.5) -- (0,4), arrow=Arrow);
real f(real x) { return (x*x*x+x)/30-1; }
real fp(real x) { return (3*x*x+1)/30; }
pen curve = linewidth(1pt);
draw(graph(f, -1, 5.1), p=curve);
real x = 5;
for (int i = 0; i < 4; ++i) {
    real y = f(x);
    draw((x,y), p=linewidth(4pt)+rgb(0.6,0,0));
    real xp = x - y / fp(x);
    draw((x,0) -- (x,y) -- (xp,0), p=rgb(0.6,0,0));
    x = xp;
}
```

# Méthode de Newton

On part d'une fonction $f:\mathbb{R}\to\mathbb{R}$ supposée **dérivable** et d'un réel $x$.

On pose une suite récurrente:
\begin{align*}
  x_0 &= x \\
  x_{n+1} &= x_n - \frac{f(x_n)}{f'(x_n)}
\end{align*}

Si tout va bien, la suite converge vers un zéro de $f$.

# Convergence de la méthode de Newton

Supposons que $f(0)=0$, que $f'(0)\neq0$ et que $f$ est $C^2$ sur un voisinage
compact $V$ de $0$.  
Alors, pour tout $x\in V$, par la formule de Taylor-Lagrange appliquée en $x$
on a
$$
  0 = f(0) = f(x) - f'(x) x + \frac{f''(y)}{2} x^2
  \quad\text{pour un certain } y\in[0,x]
$$
et donc
$$
  x - \frac{f(x)}{f'(x)}
  = \frac{f''(y)}{2 f'(x)} x^2 .
$$
Sur $V$ on peut majorer $f''(y)/2f'(x)$ par une constante $M$, alors pour tout
$n$ on a
$$
  x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)} \leq M x_n^2
  \qquad \text{donc pour tout $n$} \qquad
  x_n \leq \frac{(M x_0)^{2^n}}{M} .
$$

# Remarques sur la méthode de Newton

- La convergence est *quadratique*: on double le nombre de chiffres
  significatifs à chaque itération.

- La valeur initiale et la condition d'arrêt sont délicates à déterminer et
  nécessitent de connaître la fonction considérée.

- Si la fonction $f$ est convexe, c'est plus simple.

- Si on ne peut pas calculer la dérivée $f'$, la *méthode de la sécante* est
  une variante assez efficace: on remplace $f'(x)$ par un taux d'acroissement
  à proximité de $x$.

# TP

## Exercice

Implémenter la recherche dichotomique sous forme d'une fonction Python
`dichotomie(f,a,b)` qui renvoie un zéro de la fonction `f` compris entre `a`
et `b`, en supposant que `f(a)` et `f(b)` sont de signes opposés.

## Exercice

Implémenter la méthode de Newton sous forme d'une fonction Python
`newton(f,df,x)` qui calcule un zéro proche de `x` avec la meilleure précision
possible. Bien spécifier ce qui se passe en cas d'erreur.

\bigskip

Comparer les deux méthodes.

# Calcul de fonctions par inversion

La recherche de zéros de fonctions donne une méthode pour calculer des
fonctions réciproques: calculer $f^{-1}(x)$ revient à trouver $y$ tel que
$f(y)-x=0$.

## Exercice

Donner un algorithme de calcul de la racine carrée en utilisant la méthode de
Newton.

## Exercice

On se demande comment calculer la fonction $\ln$.

1. Montrer que la fonction $e^x$ se calcule bien à partir de son développement
   en série entière.
2. Donner une méthode pour calculer $\ln(x)$ à partir du développement en
   série entière et la tester.
3. Donner une méthode pour calculer $\ln(x)$ avec la méthode de Newton et la
   comparer à la précédente.

