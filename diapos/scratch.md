% Introduction à Scratch
% Emmanuel Beffara
% M1 MEEF maths 2023-2024, UGA

# Scratch: de quoi on parle

## Objectif

Découverte de notions fondamentales de l'informatique:

- algorithme
- langage
- machine
- information

dans un cadre adapté:

- contrôlé (tout se passe dans un environnement plutôt restreint)
- simple (prise en main relativement intuitive)
- ludique (pousse à l'expérimentation sous forme de jeu)

On ne peut pas faire toute l'informatique avec, mais on peut en faire
beaucoup.

## Historique

Les gens:

- Jean Piaget (psychologie du développement, constructivisme)
- Seymour Papert (constructionnisme, informatique éducative)
- Alan Kay & Dan Ingalls (programmation objet et graphique)
- MIT Media Lab (IA, interfaces homme-machine, didactique…)

Les langages:

- Lisp (1960)
- Logo (1970)
- Smalltalk (1972, 1980)
- Squeak (1996)
- Scratch (2002)

et les cousins Blockly, Snap!, mBlock, etc.

## Principes

Apprentissage par expérimentation personnelle:

- création d'un « micromonde » à explorer,

- le « lutin » comme objet de transition  
  entre l'expérience connue (monde physique, langage) et les concepts
  informatiques,

- l'erreur comme étape de l'apprentissage,

- la programmation structurée comme mode de pensée,

- importance de l'*affectif* dans l'apprentissage.

Programmation visuelle par *blocs* pour forcer l'expression structurée en
évitant les difficultés liées à l'écriture directe de texte dans une syntaxe
contrainte.


# L'outil

## Mise en place

Scratch 3 vit dans un navigateur web.
On peut l'utiliser en ligne ou le télécharger.

![écran d'accueil](demarrage.png){height=5cm}\ 

Version de référence en ligne:
**<https://scratch.mit.edu/projects/editor/>**

L'interface et le langage sont traduits dans de nombreuses langues.

## Présentation de l'interface

[**Sur l'écran de démarrage…**](https://scratch.mit.edu/projects/editor/)

La scène, où les choses se passent.

L'espace des objets créés:

- lutins (ou *sprites*),
- arrière-plans.

L'espace de programmation:

- palette des blocs utilisables,
- espace des scripts.

## Les lutins

Les lutins sont des objets positionnés dans l'espace, caractérisés par

- une position et une direction,
- une apparence (les *costumes*),
- des possibilités d'action:
  - se déplacer
  - dessiner
  - dire quelque chose
  - changer d'apparence
  - …
- des possibilités de perception:
  - réagir à un évènement
  - détecter le contact avec un autre lutin
  - demander quelque chose
  - …

## Les scripts

Les *scripts* sont les programmes qui fixent le comportement des lutins.

- Les blocs donnés dans la palette fournissent les opérations élémentaires.

- Des blocs sont insérés dans la zone de programmation.

- Ils sont assemblés pour former des suites d'instructions et des
  constructions algorithmiques.

- Les blocs de différentes formes représentent des notions algorithmiques
  différentes:
  - instructions et structures de contrôle (pièces de puzzle)
  - expressions numériques (blocs arrondis)
  - tests (blocs pointus)

- Les couleurs de blocs correspondent à différents domaines:
  - mouvement
  - apparence
  - calcul numérique
  - …

# Activités

## Retour au port

::: columns
:::: {.column width="40%"}
![](retour-au-port.png)

Un script est fourni au départ, il permet de faire tourner le bateau et de le
faire avancer d'un pas en utilisant le clavier.
::::

:::: {.column width="60%"}
### Consigne

1. Ajouter au programme de quoi compter le nombre de déplacements, pour
   trouver (à la main) le plus court chemin qui permette au bateau de
   rejoindre le port.

2. Modifier le programme pour empêcher un bateau coulé de continuer à avancer.

### Objectifs

1. S'approprier le concept de variable informatique de type "accumulateur"
   dans une situation problème dont la résolution nécessite sa mobilisation.

2. Découvrir les variables d'état. 
::::
:::

\bigskip

*Source: IREM des Pays de Loire, 2020*

## Tracé de figures géométriques

C'est une entrée classique, adaptée au collège  
*(même si elle n'est pas vraiment dans l'esprit initial de Scratch)*

Exercices:

- Construire un carré de côté 100.
- Généraliser à un polygône régulier quelconque.
- Réaliser des frises.
- Dessiner un alignement de carrés.

Les notions en jeu ici:

- repérage dans le le plan avec des mouvements relatifs
  (vision de l'espace *GPS* plutôt que *carte*),
- ordre des instructions,
- itération,
- prise de conscience de l'*état* du système.
