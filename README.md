# Algo701 — Algorithmique et programmation

Cette page rassemble le matériel utilisé dans l'UE Algo701 du
master [MEEF second degré](http://formations.univ-grenoble-alpes.fr/fr/catalogue/master-XB/arts-lettres-langues-ALL/master-meef-second-degre-program-master-meef-second-degre.html)
parcours [Mathématiques](http://formations.univ-grenoble-alpes.fr/fr/catalogue/master-XB/arts-lettres-langues-ALL/master-meef-second-degre-program-master-meef-second-degre/parcours-mathematiques-subprogram-parcours-mathematiques.html)
de l'[Inspé](https://inspe.univ-grenoble-alpes.fr/) de Grenoble.

## Présentation

Objectifs:

> Permettre aux futurs professeurs d’avoir une démarche algorithmique sur des
> notions mathématiques enseignées au lycée et au collège. Travail sur la
> programmation et la maîtrise des outils informatiques. Une part importante
> de cet enseignement est effectuée sous forme de TP sur machines.

Responsable: Emmanuel Beffara

Pour les activités en Python, je fournis un carnet pour
[Jupyter](https://jupyter.org/), sous forme d'un fichier au format `.ipynb`.
Vous pouvez le télécharger et l'utiliser avec Jupyter sur votre propre machine
ou avec la [plateforme Jupyterhub de l'UGA](https://jupyterhub.u-ga.fr/), sur
laquelle vous pouvez vous connecter avec vos identifiants usuels. Je fournis
aussi une version lisible non interactive.

Pour les activités en Scratch, vous pouvez utiliser le
[site officiel](https://scratch.mit.edu/projects/editor/) pour expérimenter.

## Plan

### 11 septembre 2023: introduction

- L'informatique comme outil ou comme science, la pensée algorithmique
  ([diaporama](diapos-intro.pdf))
- La recette de Kaprekar

### 18 septembre 2023: introduction à Scratch

- Présentation de Scratch ([diaporama](diapos-scratch.pdf))
- TP: Retour au port, Tracé de frises

### 25 septembre 2023: variables et invariants en Scratch

- TP quadrilatères

### 2 octobre 2023: optimisation combinatoire

- Problème du rendu de monnaie ([HTML](tp-monnaie.html), [PDF](tp-monnaie.pdf))
- Éléments de correction ([HTML](tp-monnaie-correction.html),
  [PDF](tp-monnaie-correction.pdf))

### 9 et 16 octobre 2023: calcul de racines carrées

- Présentation des méthodes par balayage, dichotomie, méthode de Héron:
  [diaporama](diapos-racine.pdf)
- TP Python ([HTML](tp-racine.html), [PDF](tp-racine.pdf),
  [Jupyter](tp-racine.ipynb))

### devoir maison pour le 9 novembre

### 13 novembre 2023: aléatoire en algorithmique

- Aléatoire en algorithmique: [diaporama](diapos-aleatoire.pdf)
- TP Python ([HTML](tp-aleatoire.html), [PDF](tp-aleatoire.pdf),
  [Jupyter](tp-aleatoire.ipynb))

### 4 décembre 2023: chaînes de caractères et programmes de dessin

- TP Python ([HTML](tp-dessin.html), [PDF](tp-dessin.pdf),
  [Jupyter](tp-dessin.ipynb))

### 11 décembre 2023: TP Python noté

- Sujet 2022: calculs d'enveloppes convexes
  ([HTML](tp-enveloppes.html), [PDF](tp-enveloppes.pdf), [Jupyter](tp-enveloppes.ipynb))

### 18 décembre 2023: contrôle sur table

- Voir le [sujet du contrôle 2022](examen.pdf)
