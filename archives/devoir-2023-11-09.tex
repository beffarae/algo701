\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\newif\ifanswers
\ifdefined\ANSWERS \answerstrue \else \answersfalse \fi
\answerstrue

\ifanswers
  \title{Algo 701 -- Correction du devoir}
\else
  \title{Algo 701 -- Devoir à la maison}
\fi
\author{M1 MEEF maths 2023--2024, UGA}
\date{à faire individuellement pour le 9 novembre 2023}

\usepackage{amsmath}
\usepackage[french]{babel}
\usepackage{enumitem}
\usepackage{fancyhdr}
\usepackage[a4paper,margin=3cm]{geometry}
\usepackage{lastpage}
\usepackage{listings}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{newtx}
\usepackage{tikz}
\usepackage[small]{titlesec}

\pagestyle{fancy}
\fancyhead{}
\renewcommand\headrulewidth{0pt}
\fancyfoot[C]{Page \thepage\ sur \pageref{LastPage}}

\lstset{
  language=Python,
  columns=flexible,
  basicstyle=\sffamily
}

\ifanswers
\newenvironment{answer}{%
\begin{quotation}%
  \em\noindent\ignorespaces
}{%
\end{quotation}%
}
\else
\newenvironment{answer}{%
\begingroup\setbox0=\vbox\bgroup
\begin{quotation}\em\noindent\ignorespaces}{%
\end{quotation}\egroup\endgroup}
\fi

\setlist[enumerate,1]{label=\textbf{Q\arabic*.}}
\setlist[enumerate,2]{label=\textbf{(\alph*)}}

\newcommand\md{\mathbin{\%}}
\DeclareMathOperator\pgcd{PGCD}
\newcommand\abs[1]{\left|{#1}\right|}

\begin{document}
\maketitle
\thispagestyle{fancy}

\section{PGCD binaire}

Le classique algorithme d'Euclide procède par divisions euclidiennes
successives pour calculer le PGCD de deux entiers, utilisant la
propriété que $\pgcd(a,b)=\pgcd(a\md b,b)$ pour tous entiers $a$ et $b$ non
nuls, où $a\md b$ désigne le reste de la division euclidienne de $a$ par $b$.
Sur la plupart des ordinateurs, l'opération générale de division euclidienne
est plutôt coûteuse en temps par rapport à la division par $2$ (qui consiste à
supprimer le chiffre des unités en binaire), le test de parité (qui consiste à
tester le chiffre des unités) et la soustraction. On étudie ici une variante
de l'algorithme d'Euclide qui n'utilise que ces opérations.
Tout se passe dans les entiers relatifs.

\begin{enumerate}
  \item On établit d'abord quelques propriétés arithmétiques.
    On considère deux entiers $a$ et $b$ non nuls.
    \begin{enumerate}
      \item Montrer que si $a$ et $b$ sont pairs, alors
        $\pgcd(a,b)=2\times\pgcd(a/2,b/2)$.
        \begin{answer}
          De façon générale, pour tous entiers $k$ non nul, $u$ et $v$, on a
          $\pgcd(ku,kv)=k\,\pgcd(u,v)$.
          Le résultat attendu correspond au cas où $k=2$, $u=a/2$ et $v=b/2$.
        \end{answer}
      \item Montrer que si $a$ est impair et $b$ est pair, alors
        $\pgcd(a,b)=\pgcd(a,b/2)$.
        \begin{answer}
          Si $a$ est impair, tout diviseur commun de $a$ et $b$ est
          impair et est donc un diviseur de $b/2$.
          Par ailleurs, tout diviseur de $b/2$ est diviseur de $b$.
          Par conséquent, les diviseurs communs de $a$ et $b$ sont les
          diviseurs communs de $a$ et $b/2$, le plus grand d'entre eux est
          donc le même.
        \end{answer}
      \item Montrer que si $a$ et $b$ sont impairs, alors
        $\pgcd(a,b)=\pgcd((a-b)/2,b)$.
        \begin{answer}
          Pour tous entiers $a$ et $b$ on a $\pgcd(a,b)=\pgcd(a-b,b)$ puisque
          les diviseurs communs de $a$ et $b$ sont les mêmes que les diviseurs
          communs de $a-b$ et $b$.
          Si $a$ et $b$ sont impairs, alors $a-b$ et pair et la question
          précédente s'applique, on a donc $\pgcd(a-b,b)=\pgcd((a-b)/2,b)$.
        \end{answer}
      \item Montrer que $\pgcd(a,0)=\abs{a}$.
        \begin{answer}
          Pour tout entier $d$ on a $d\times 0=0$, donc $d$ divise $0$.
          Par conséquence, l'ensemble des diviseurs communs de $a$ et $0$ est
          l'ensemble des diviseurs de $a$.
          Le plus grand diviseur de $a$ est $\abs{a}$, donc
          $\pgcd(a,0)=\abs{a}$.
        \end{answer}
    \end{enumerate}
\end{enumerate}
On considère l'algorithme suivant, qui prétend calculer le PGCD:
\begin{tabbing}
  \qquad\=\qquad\=\qquad\=\kill
  \>Entrées: deux entiers $a$ et $b$ \\
  \>Sortie: le PGCD de $a$ et $b$ \\
  \>Variable auxiliaire: $m$, entier \\[1ex]
  \>$m \leftarrow 1$ \\
  \>Tant que $b\neq 0$, faire: \\
  \>\> si $a$ et $b$ sont pairs, alors \\
  \>\>\> $m \leftarrow 2 \times m$ \\
  \>\>\> $a \leftarrow a/2$ \\
  \>\>\> $b \leftarrow b/2$ \\
  \>\> sinon, si $a$ est impair et $b$ est pair, alors \\
  \>\>\> $b \leftarrow b/2$ \\
  \>\> sinon, si $a$ et $b$ sont impairs, alors \\
  \>\>\> $b \leftarrow (b-a)/2$ \\
  \>Renvoyer $a$.
\end{tabbing}
Cet algorithme est incorrect, le but des questions suivantes est de le
corriger.
On précise que les divisions présentes dans l'algorithme désignent
toujours le quotient dans la division euclidienne (on n'est pas en Python,
tout se passe dans les entiers).
\begin{enumerate}[resume]
  \item On commence par mettre en évidence que le comportement de
    l'algorithme présenté ci-dessus n'est pas toujours celui qui serait
    attendu.
    \begin{enumerate}
      \item Donner un couple de valeurs d'entrée $(a,b)$ pour lequel
        l'algorithme renvoie bien $\pgcd(a,b)$.
        \begin{answer}
          Par exemple, avec $a=3$ et $b=6$, on a le déroulé suivant:
          \[
            \begin{array}{cccl}
              \text{itérations} & a & b & \text{opération} \\
              0 & 3 & 6 & b \leftarrow b/2 \\
              1 & 3 & 3 & b \leftarrow (b - a)/2 \\
              2 & 3 & 0 & \text{renvoyer $a$}
            \end{array}
          \]
          et la valeur renvoyée est bien $3$.
        \end{answer}
      \item Donner un couple de valeurs d'entrée $(a,b)$ pour lequel
        l'algorithme renvoie un entier qui n'est pas $\pgcd(a,b)$.
        \begin{answer}
          Par exemple, avec $a=2$ et $b=6$, on a le déroulé suivant:
          \[
            \begin{array}{cccl}
              \text{itérations} & a & b & \text{opération} \\
              0 & 2 & 6 & m \leftarrow 2 \times m, a \leftarrow a/2, b \leftarrow b/2 \\
              1 & 1 & 3 & b \leftarrow (b - a)/2 \\
              3 & 1 & 1 & b \leftarrow (b - a)/2 \\
              4 & 1 & 0 & \text{renvoyer $a$}
            \end{array}
          \]
          et la valeur renvoyée est $1$ alors que $\pgcd(2,6)=2$.
        \end{answer}
      \item Donner un couple de valeurs d'entrée $(a,b)$ pour lequel
        l'algorithme ne termine pas.
        \begin{answer}
          Par exemple, avec $a=3$ et $b=2$, on a le déroulé suivant:
          \[
            \begin{array}{cccl}
              \text{itérations} & a & b & \text{opération} \\
              0 & 3 & 2 & b \leftarrow b/2 \\
              1 & 3 & 1 & b \leftarrow (b - a)/2 \\
              2 & 3 & -1 & b \leftarrow (b - a)/2 \\
              3 & 3 & -2 & b \leftarrow b/2 \\
              4 & 3 & -1 & \cdots
            \end{array}
          \]
          et l'exécution continuera à tourner en rond, avec $b$ qui alterne
          entre $-1$ et $-2$.
        \end{answer}
    \end{enumerate}
    Chaque cas devra être justifié.
  \item On pose l'invariant suivant, où $a_0$ et $b_0$ désignent les
    valeurs initiales de $a$ et $b$, celles données en entrée:
    \[
      I : \quad m \times \pgcd(a, b) = \pgcd(a_0, b_0)
    \]
    Montrer que $I$ est satisfait lors de l'entrée dans la boucle et que
    le corps de la boucle le préserve, c'est-à-dire que si $I$ est
    satisfait au début d'une itération alors il est satisfait aussi à la
    fin de cette itération.
    \begin{answer}
      Lors de l'entrée dans la boucle, on a $m=1$ d'après la première
      instruction et $a=a_0$ et $b=b_0$ par définition, donc $I$ est satisfait.

      Le fait que le corps de la boucle préserve $I$ est une conséquence directe
      des résultats de la première question.
      Dans le cas où $a$ et $b$ sont pairs, on a $\pgcd(a,b)=2\pgcd(a/2,b/2)$
      donc le fait de multiplier $m$ par $2$ fait que $m\times\pgcd(a,b)$ ne
      change pas.
      Dans les deux autres cas, $\pgcd(a,b)$ ne change pas et $m$ non plus donc
      $m\times\pgcd(a,b)$ est invariant.
    \end{answer}
  \item Corriger la ligne \og Renvoyer $a$\fg\ pour que l'algorithme
    renvoie $\pgcd(a_0,b_0)$.
    Justifier qu'avec cette correction, si on suppose que la boucle
    termine, la valeur renvoyée est bien celle attendue.
    \begin{answer}
      D'après la question précédente, l'invariant $I$ est satisfait à l'entrée
      dans la boucle et préservé par chaque itération donc il est satisfait en
      sortie de boucle, si la boucle termine.
      Par ailleurs, la condition d'arrêt donne $b=0$ en sortie de boucle, d'où
      $\pgcd(a,b)=\abs{a}$ dans ce cas, et on a donc
      $\pgcd(a_0,b_0)=m\times\pgcd(a,b)=m\times\abs{a}$ en sortie de boucle.
      En corrigeant la ligne en \og Renvoyer $m\times\abs{a}$\fg on obtient donc
      une valeur de retour correcte.
    \end{answer}
\end{enumerate}
Avec la correction apportée par la question précédente, on obtient un
algorithme qui ne termine pas toujours, mais qui renvoie bien la valeur
attendue quand il termine.
\begin{enumerate}[resume]
  \item Proposer un modification de l'algorithme qui garantisse que
    l'itération termine toujours.
    L'algorithme modifié ne doit utiliser que des tests de parité, des
    divisions par $2$ et des soustractions, comme l'algorithme proposé au
    départ.
    Pour ce nouvel algorithme,
    \begin{enumerate}
      \item montrer que l'invariant $I$ est toujours valable,
      \item montrer que l'algorithme termine toujours.
    \end{enumerate}
    \begin{answer}
      Il faut au moins traiter le cas manquant dans l'algorithme initial,
      c'est-à-dire le cas où $a$ et pair et $b$ impair, sinon c'est un cas où
      ni $a$ ni $b$ ne sont modifiés et où l'itération ne peut donc pas
      terminer.
      Par ailleurs, pour garantir la terminaison, on peut s'assurer que $a$ et
      $b$ restent positifs dans le dernier cas (alors que la formulation
      initiale pouvait donner des nombres négatifs).
      Pour garantir cette propriété au départ, on peut commencer par remplacer
      les deux entrées par leur valeur absolue, ce qui par ailleurs dispense
      du calcul de valeur absolue à la dernière ligne.
      On obtient alors l'algorithme suivant:
      \begin{tabbing}
        \qquad\=\qquad\=\qquad\=\qquad\=\kill
        \>Entrées: deux entiers $a$ et $b$ \\
        \>Sortie: le PGCD de $a$ et $b$ \\
        \>Variable auxiliaire: $m$, entier \\[1ex]
        \>$a \leftarrow \abs{a}$ \\
        \>$b \leftarrow \abs{b}$ \\
        \>$m \leftarrow 1$ \\
        \>Tant que $b\neq 0$, faire: \\
        \>\> si $a$ et $b$ sont pairs, alors \\
        \>\>\> $m \leftarrow 2 \times m$ \\
        \>\>\> $a \leftarrow a/2$ \\
        \>\>\> $b \leftarrow b/2$ \\
        \>\> sinon, si $a$ est impair et $b$ est pair, alors \\
        \>\>\> $b \leftarrow b/2$ \\
        \>\> sinon, si $a$ est pair et $b$ est impair, alors \\
        \>\>\> $a \leftarrow a/2$ \\
        \>\> sinon, si $a$ et $b$ sont impairs, alors \\
        \>\>\> si $a<b$, alors \\
        \>\>\>\> $b \leftarrow (b-a)/2$ \\
        \>\>\> sinon \\
        \>\>\>\> $a \leftarrow (a-b)/2$ \\
        \>Renvoyer $m\times a$.
      \end{tabbing}
      On vérifie facilement que l'invariant est toujours préservé, par ailleurs
      l'invariant \og $a\geq 0$ et $b\geq 0$\fg\ est aussi garanti à l'entrée
      dans la boucle et préservé par celle-ci.
      Par construction, au moins un entier parmi $a$ et $b$ décroît strictement
      à chaque itération, donc la terminaison est garantie.
    \end{answer}
  \item Montrer que le nombre d'itérations de cet algorithme est au plus
    $\log_2(ab)+1$.
    \begin{answer}
      Le produit $ab$ est au moins divisé par $2$ à chaque itération sauf si
      $ab$ est nul.
      Après $n$ itérations, on a donc $ab\leq\abs{a_0b_0}/2^n$.
      Si après ces $n$ itérations on a $ab\neq0$, on a donc
      $0\leq\log_2(ab)\leq\log_2(a_0b_0)-n$, d'où $n\leq\log_2(a_0b_0)$.
      On peut avoir besoin d'une itération supplémentaire pour atteindre la
      condition d'arrêt où $ab=0$, d'où un nombre total d'itérations de
      $\log_2(a_0b_0)+1$ au maximum.
    \end{answer}
  \item Bonus: écrire une fonction en Python qui réalise l'algorithme correct.
    \begin{answer}
      En transcrivant l'algorithme corrigé, on obtient par exemple:
      \begin{lstlisting}[gobble=8]
        def pgcd(a, b):
            a = abs(a)
            b = abs(b)
            m = 1
            while b != 0:
                if a % 2 == 0:
                    a //= 2
                    if b % 2 == 0:
                        b //= 2
                        m *= 2
                elif b % 2 == 0:
                    b //= 2
                elif a < b:
                    b = (b - a) // 2
                else:
                    a = (a - b) // 2
            return m * a
    \end{lstlisting}
    On remarquera que, dans ce code, les structures conditionnelles sont un
    peu réorganisées pour éviter de dupliquer des tests et de trop
    imbriquer, mais la logique est la même.
    La division \lstinline{a //= 2} n'est écrite qu'une seule fois parce
    qu'elle a lieu dans les deux cas où \lstinline{a} est pair; le test qui
    suit sur la parité de \lstinline{b} doit bien être imbriqué et il ne
    reste rien à faire dans le cas où il est faux.
  \end{answer}
\end{enumerate}

\section{Tranche de somme minimale}

On se donne un tableau de nombres entiers relatifs:
\[
  T = \bigl[ T[1], T[2], \ldots , T[n] \bigr]
\]
On appelle \emph{tranche} de $T$ une suite d'éléments consécutifs du tableau,
on note $T[g\ldots d]$ la tranche $\bigl[T[g],T[g+1],\ldots,T[d]\bigr]$ qui
s'étend de l'indice $g$ à l'indice $d$ inclus.
On demande de trouver une tranche de $T$ dont la somme des éléments est
minimale.

Par exemple, pour $T=[-1, -1, 3, 5, -9, 3, -10, 5]$, la tranche
$T[5\ldots7]=[-9, 3, -10]$ est de somme minimale parce que sa somme est $-16$
et qu'aucune tranche de $T$ n'a de somme inférieure.

\begin{enumerate}
  \item L'algorithme suivant énumère toutes les tranches qui ont une borne
    gauche donnée et retient celle dont la somme est minimale:
    \begin{tabbing}
      \qquad\=\qquad\=\qquad\=\qquad\=\kill
      \>Algorithme \og tranche de somme minimale de $T$ de borne gauche $g$\fg \\
      \>Entrées: un tableau $T$ de longueur $n$,
      un indice $g$ avec $1\leq g\leq n$ \\
      \>Sortie: un couple $(d,s)$ tel que $T[g\ldots d]$ est de somme $s$
      minimale \\\>\> (parmi les tranches de $T$ de borne gauche $g$) \\
      \>Variables: deux entiers $s$ et $m$, deux indices 
      $i$ et $d$ \\[1ex]
      \>$s \leftarrow T[g]$ \\
      \>$m \leftarrow s$ \\
      \>$d \leftarrow g$ \\
      \>$i \leftarrow g$ \\
      \>Tant que $i < n$, faire: \\
      \>\>$i \leftarrow i + 1$ \\
      \>\>$s \leftarrow s + T[i]$ \\
      \>\>si $s < m$, alors \\
      \>\>\>$d \leftarrow i$ \\
      \>\>\>$m \leftarrow s$ \\
      \>Renvoyer $(d,m)$.
    \end{tabbing}
    On appelle $I_1$ l'assertion suivante:
    \begin{quote}
      D'une part $s$ est la somme de $T[g\ldots i]$.
      D'autre part, parmi les tranches de la forme $T[g\ldots k]$ avec
      $k\leq i$, $T[g\ldots d]$ est de somme minimale et sa somme est $m$.
    \end{quote}
    En utilisant l'invariant $I_1$, montrer que cet algorithme est correct.
    \begin{answer}
      Lors de l'entrée dans la boucle, les variables sont initialisées avec
      $i=d=g$ et $m=s=T[g]$, donc $s$ est bien la somme de
      $T[g\ldots i]=T[g\ldots g]=\bigl[T[g]\bigr]$, de plus cette tranche est
      la seule de la forme $T[g\ldots k]$ avec $k\leq i$ puisque $g$ est la
      seule valeur possible pour $k$, donc c'est bien la tranche de somme
      minimale parmi celles-ci.
      Ainsi $I_1$ est satisfait à l'entrée dans la boucle.

      Supposons maintenant que $I_1$ est satisfait au début d'une itération,
      donc avec $g\leq i<n$ pour un certain $i$, et vérifions qu'il est
      satisfait en fin d'itération.
      Notons $s',m',d',i'$ les valeurs des variables en fin d'itération, pour
      les différencier des valeurs au début de l'itération qui seront notées
      $s,m,d,i$, de sorte qu'on aura $i'=i+1$.
      L'ensemble de tranches considérées contiendra une tranche de plus, à
      savoir $T[g\ldots i']$.
      L'instruction $s\leftarrow s+T[i]$, exécutée après $i\leftarrow i+1$,
      garantit que $s'$ sera bien la somme de $T[g\ldots i']$.
      De plus, une tranche de somme minimale parmi celle considérée dans
      l'invariant sera
      \begin{itemize}
        \item soit $T[g\ldots i+1]$ si sa somme $s'$ est strictement
          inférieure à toutes celles de la forme $T[g\ldots k]$ avec
          $k\leq i$, ce qui est équivalent à dire que sa somme est strictement
          inférieure à $m$ qui est par hypothèse la somme minimale parmi
          celles-ci,
        \item soit une tranche de somme minimale parmi celles pour lesquelles
          $k\leq i$, dans le cas où la somme $s'$ de $T[g\ldots i+1]$ est
          supérieure ou égale à celle d'une tranche de somme minimale avec
          $k\leq i$, dont la somme est $m$ par hypothèse.
      \end{itemize}
      Le test $s<m$ permet donc bien de distinguer entre ces deux cas, et les
      deux instructions indiquées dans le cas où ce test est vrai donnent
      $d'=i'$ et $m'=s'$, ce qui rétablit l'invariant comme attendu.
      Dans l'autre cas on a $d'=d$ et $m'=m$, ce qui est valide d'après la
      remarque précédente.

      Ainsi l'invariant est satisfait en cas de terminaison.
      De plus, la valeur de $i$ est entière et croît strictement alors que $n$
      est constant, donc la condition de boucle $i<n$ finira par être fausse
      et l'itération terminera, avec $i\geq n$ comme condition d'arrêt.
      Par ailleurs, puisque $i$ est initialisé avec $g\leq n$ et qu'il est
      incrémenté de $1$ à chaque étape, on aura en fait $i=n$ en fin de
      boucle.
      Comme l'invariant est préservé, par définition, $T[g\ldots d]$ sera bien
      de somme minimale $m$ avec $g\leq d\geq n$ et le couple $(d,n)$
      correspondra bien à la spécification.

      Notons que l'algorithme donne \emph{une} tranche de somme minimale, il
      peut y en avoir plusieurs de même somme.
      De fait, si le test était remplacé par $s\leq m$, l'algorithme serait
      valide aussi mais il renverrait la plus grande valeur possible pour $d$
      alors qu'avec le test $s<m$ il renverra la plus petite.
    \end{answer}
  \item On donne l'algorithme suivant, qui utilise celui de la question
    précédente:
    \begin{tabbing}
      \qquad\=\qquad\=\qquad\=\qquad\=\kill
      \>Entrées: un tableau $T$ de longueur $n$ \\
      \>Sortie: un couple $(g,d)$ tel que $T[g\ldots d]$ est de somme
      minimale \\
      \>Variables: deux entiers $s$ et $m$, quatre indices 
      $i,j,g,d$ \\[1ex]
      \>$(d,m) \leftarrow \text{tranche de somme minimale de $T$ de borne gauche $1$}$ \\
      \>$g \leftarrow 1$ \\
      \>$i \leftarrow 1$ \\
      \>Tant que $i < n$, faire: \\
      \>\>$i \leftarrow i + 1$ \\
      \>\>$(j,s) \leftarrow \text{tranche de somme minimale de $T$ de borne gauche $i$}$ \\
      \>\>si $s < m$, alors \\
      \>\>\>$g \leftarrow i$ \\
      \>\>\>$d \leftarrow j$ \\
      \>\>\>$m \leftarrow s$ \\
      \>Renvoyer $(g,d)$.
    \end{tabbing}
    Montrer que cet algorithme est correct, en utilisant un invariant bien
    choisi.
    \begin{answer}
      On peut poser l'invariant $I_2$ suivant:
      \begin{quote}
        La tranche $T[g\ldots d]$ est de somme $m$ minimale parmi les tranches
        $T[k\ldots\ell]$ avec $1\leq k\leq i$ et $k\leq\ell\leq n$.
      \end{quote}
      D'après la question précédente, l'initialisation donne $d$ et $m$ tels
      que $T[1\ldots d]$ est de somme minimale parmi les tranches
      $T[1\ldots\ell]$ avec $1\leq\ell n$, ce qui correspond bien à $I_2$ dans
      le cas $g=i=1$ puisque $1$ est la seule valeur de $k$ qui satisfait
      $1\leq k\leq i$ dans ce cas.

      Supposons maintenant que $I_2$ est satisfait au début d'une itération et
      montrons qu'il l'est en fin d'itération.
      Comme au dessus, on notera $m',i',g',d'$ les valeurs des variables
      en fin d'itération.
      On a $i'=i+1$ donc l'ensemble de tranches à considérer en fin
      d'itération contient en plus celles de la forme $T[i'\ldots ell]$ avec
      $i'\leq\ell\leq n$.
      Une tranche minimale parmi celles concernées par $I_2$ en fin
      d'itération peut donc être
      \begin{itemize}
        \item soit de la forme $T[i'\ldots j]$ avec $i'\leq j\leq n$, de somme
          $s$ inférieure à celle de toutes les tranches $T[k\ldots\ell]$ avec
          $1\leq k\leq i$ et $k\leq\ell\leq n$, et ce cas est caractérisé par
          $s<m$ par l'hypothèse que $I_2$ est satisfait en début d'itération,
        \item soit de somme minimale parmi celles de la forme $T[k\ldots\ell]$
          avec $1\leq k\leq i$ et $k\leq\ell\leq n$, et dans ce cas sa somme
          soit être $m$ et $T[g\ldots d]$ convient.
      \end{itemize}
      Dans le second cas, $g,d,m$ restent inchangés et $I_2$ est satisfait en
      fin d'itération.
      Dans le premier cas, l'appel à l'algorithme précédent donne $j$ et $s$
      tels que la tranche $T[i'\ldots j]$ est de somme $s$ minimale, alors les
      trois affectations donnent $g'=i'$, $d'=j$ et $m'=s$ ce qui garantit que
      $I_2$ est satisfait dans ce cas également.

      La terminaison est garantie par le même argument que précédemment et
      donne $i=n$ en fin de boucle.
      Comme $I_2$ est satisfait en fin de boucle, on a bien que
      $T[g\ldots d]$ est de somme minimale $m$.
    \end{answer}
  \item Donner une estimation du nombre total d'additions effectuées lors de
    l'application de cet algorithme, en fonction de la taille $n$ du tableau
    $T$.
    \begin{answer}
      Le premier algorithme fait $n-g$ itérations dans lesquelles il fait $2$
      additions, ce qui donne donc $2(n-g)$ additions.

      Le second algorithme utilise le premier d'abord avec $g=1$, ce qui donne
      $2(n-1)$ additions, puis fait $n-1$ itérations au cours desquelles il
      fait une addition pour incrémenter $i$ et un appel au premier algorithme
      avec une borne gauche $i$ qui prendra une fois chaque valeur entre $2$
      et $n$.
      Le nombre d'additions correspondant sera donc $2(n-i)$ pour chacun des
      $i$ de $2$ à $n$.

      En appelant $A(n)$ le nombre total d'additions pour un tableau de
      taille $n$, on a donc 
      \begin{align*}
        A(n)
        &= 2(n-1) + \sum_{i=2}^n \bigl( 1 + 2(n-i) \bigr) \\
        &= (n - 1) + 2 \sum_{i=1}^n (n-i)
        &&\text{en sortant $1$ et en intégrant $2(n-1)$ comme $i=1$} \\
        &= (n - 1) + 2 \sum_{j=0}^{n-1} j
        &&\text{par changement de variable $j=n-i$} \\
        &= (n - 1) + 2 \frac{n (n-1)}{2}
        &&\text{par la formule classique} \\
        &= (n - 1) + n (n - 1)
        = n^2 - 1 .
      \end{align*}
      On a donc exactement $n^2-1$ additions.
    \end{answer}
\end{enumerate}

On va maintenant chercher à résoudre le problème en parcourant le tableau une
seule fois et en faisant donc un nombre d'additions proportionnel à $n$.
L'algorithme obtenu contiendra donc une seule boucle pour un indice $i$ allant
de $1$ à $n$.
Après avoir traité les éléments jusqu'au rang $i$, on doit avoir extrait assez
d'information pour répondre au sous-problème associé à la tranche
$T[1\ldots i]$:
\begin{center}\begin{tikzpicture}[x=2.5em,y=2em]
  \fill[red,fill opacity=0.2] (2.5,-0.5) rectangle (5.5,0.5);
  \fill[fill opacity=0.2] (8.5,-0.5) rectangle (11.5,0.5);
  \draw[thick] (-0.5,-0.5) rectangle (11.5,0.5);
  \foreach \x/\i in {0/1,1/2,3/g,5/d,8/i,11/n} \node at (\x,0) {$T[\i]$};
  \foreach \x in {2,6.5,9.5} \node at (\x,0) {$\cdots$};
  \foreach \x in {0.5,1.5,2.5,5.5,7.5,10.5} \draw[thin] (\x,-0.5)--(\x,0.5);
  \draw[red!70!black,thick] (8.5,-1) -- (8.5,1);
  \node at (4,1) {partie traitée};
  \node at (10,1) {reste};
\end{tikzpicture}\end{center}
On est donc amené à se demander, en supposant que $T[g\ldots d]$ est une
tranche de somme minimale de $T[1\ldots i]$, comment déduire une tranche
de somme minimale de
$T[1\ldots i+1]$.
\begin{enumerate}[resume]
  \item 
    On se place dans un état intermédiaire: le problème est traité pour la
    tranche $T[1\ldots i]$ pour un certain $i$ avec $1\leq i<n$, on suppose
    trouvés $g$ et $d$ tels que $T[g\ldots d]$ est une tranche de somme
    minimale de $T[1\ldots i]$.
    Pour trouver une tranche de somme minimale de $T[1\ldots i+1]$, on
    distingue deux cas:
    \begin{itemize}
      \item
        soit $T[g\ldots d]$ est aussi une tranche de somme minimale pour
        $T[1\ldots i+1]$,
      \item
        soit il y a dans $T[1\ldots i+1]$ une tranche de somme strictement
        inférieure à celle de $T[g\ldots d]$.
    \end{itemize}
    Dans le deuxième cas, la tranche de somme minimale est nécessairement de
    la forme $T[k\ldots i+1]$, sinon elle serait une tranche de
    $T[1\ldots i]$.
    \begin{enumerate}
      \item En supposant qu'on est dans le second cas, donner une
        caractérisation de $k$ qui ne dépend que des valeurs présentes dans
        $T[1\ldots i]$.
        \begin{answer}
          L'indice $k$ est défini comme celui qui minimise la somme de
          $T[k\ldots i+1]$ parmi les valeurs telles que $1\leq k\leq i+1$.
          Si $k\leq i$, la somme de cette tranche est égale à la somme de
          $T[k\ldots i]$ augmentée de $T[i+1]$.
          Si $k=i+1$, la somme est simplement $T[i+1]$.
          On peut donc caractériser $k$ comme l'indice qui donne une tranche
          $T[k\ldots i]$ de somme minimale si celle-ci est négative, ou $i+1$
          si cette somme minimale est positive ou nulle.
          Cette caractérisation ne dépend que des valeurs dans $T[1\ldots i]$.
        \end{answer}
      \item Donner un critère permettant de déterminer si on est dans le
        premier ou dans le second cas.
        \begin{answer}
          Si une tranche $T[g\ldots d]$ est de somme minimale $m$ dans
          $T[1\ldots i]$ et si $f$ est la somme minimale d'une tranche
          $T[k\ldots i]$ telle que caractérisée dans la question précédente
          (en posant $f=0$ si $k=i+1$), alors la somme minimale d'une tranche
          dans $T[1\ldots i+1]$ est le minimum entre $m$ et $f+T[i+1]$.
          Le critère est donc $m<f+T[i+1]$ pour être dans le premier cas (là
          encore, l'inégalité peut être prise stricte ou large, cela ne fera
          de différence que lorsque les deux cas sont admissibles parce qu'ils
          permettraient d'obtenir le même minimum de deux façons différentes).
        \end{answer}
    \end{enumerate}
  \item
    On va maintenant exploiter cet analyse pour produire un algorithme
    efficace.
    On pose comme variables auxiliaires des indices $i,g,d,k$ et des entiers
    $m$ et $f$.
    On pose les invariants suivants:
    \begin{itemize}
      \item $g$ et $d$ sont les bornes d'une tranche minimale de
        $T[1\ldots i]$ de somme $m$,
      \item $T[k\ldots i]$ est une tranche de somme $f$ minimale parmi les
        tranches de borne droite $i$.
    \end{itemize}
    Écrire un algorithme utilisant ces variables et invariants pour déterminer
    une tranche de somme minimale de $T$ en un seul parcours de $T$.
    \begin{answer}
      En suivant l'analyse de la question précédente, on peut formuler
      l'algorithme suivant:
      \begin{tabbing}
        \qquad\=\qquad\=\qquad\=\kill
        \>Entrée: un tableau $T$ de longueur $n$ \\
        \>Sortie: un couple $(g,d)$ tel que $T[g\ldots d]$ est de somme
        minimale \\
        \>Variables: deux entiers $m$ et $f$, quatre indices $i,g,d,k$ \\[1ex]
        \>$g \leftarrow 1$, $d \leftarrow 1$, $m \leftarrow T[1]$ \\
        \>$k \leftarrow 1$, $f \leftarrow T[1]$ \\
        \>$i \leftarrow 1$ \\
        \>Tant que $i<n$, faire: \\
        \>\>$i \leftarrow i + 1$ \\
        \>\>si $f\geq 0$, alors \\
        \>\>\>$f \leftarrow T[i]$, $k \leftarrow i$ \\
        \>\>sinon \\
        \>\>\>$f \leftarrow f + T[i]$, \\
        \>\>si $f < m$ alors \\
        \>\>\>$m \leftarrow f$, $g \leftarrow k$, $d \leftarrow i$. \\
        \>Renvoyer $(g,d)$.
      \end{tabbing}
    \end{answer}
  \item
    Démontrer la correction de l'algorithme proposé.
    \begin{answer}
      L'invariant proposé est satisfait à l'entrée dans la boucle puisque pour
      $i=1$ il n'y a que $T[1\ldots 1]$ comme seule tranche à considérer et
      qu'elle est donc de somme minimale $T[1]$.

      Ensuite, supposons l'invariant satisfait au début d'une itération.
      Comme précédemment, on note $i',g',d',k',m',f'$ les valeurs des
      variables en fin d'itération.
      La première conditionnelle permet de garantir que $T[k'\ldots i']$ est
      de somme minimale $f'$ parmi les tranches de borne droite $i'$, car elle
      applique le critère identifié à la question précédente.
      À l'issue de cette étape, on a donc une tranche $T[g\ldots d]$ de somme
      $m$ minimale parmi les tranches contenues dans $T[1\ldots i]$ et une
      tranche $T[k'\ldots i']$ de somme $f'$ minimale parmi les tranches de
      borne droite $i'=i+1$.
      Le minimum des tranches de $T[1\ldots i']$ est donc $\min(m,f')$ et la
      deuxième conditionnelle permet de mettre à jour $m,g,d$ dans le cas où
      $f'<m$.
      Ainsi l'invariant est préservé.

      Enfin, l'indice $i$ est initialisé à $1$ et est incrémenté de $1$ à
      chaque itération tant que $i<n$, donc l'itération termine nécessairement
      avec $i=n$ après $n-1$ itérations.
      En sortie de boucle, l'invariant est donc satisfait et on a $i=n$.
      La tranche $T[g\ldots d]$ est donc bien de somme minimale et le couple
      $(g,d)$ qui est renvoyé satisfait la spécification.
    \end{answer}
  \item
    Démontrer que le nombre d'additions effectuées lors de l'application de
    cet algorithme est majoré par une fonction affine de $n$.
    \begin{answer}
      La boucle fait $n-1$ itérations, comme on l'a observé plus haut.
      De plus, à chaque itération, il y a une addition pour incrémenter $i$ et
      possiblement une addition pour mettre à jour $f$, dans le cas où
      $f\geq 0$.
      Le nombre total d'additions est donc compris entre $n-1$ (cas où $f$ est
      toujours positif, alors la tranche renvoyée est réduite à un élément qui
      est le minimum du tableau) et $2(n-1)$ (cas où $f$ est toujours négatif,
      alors la tranche renvoyée est $T[1\ldots d]$ pour le plus petit $d$ qui
      minimise la somme de cette tranche).
      Si $A'(n,T)$ est le nombre d'additions pour le tableau $T$ de taille $n$
      (en effet, cette fonction dépend maintenant du tableau), on a donc
      $n-1\leq A'(n,T)\leq 2(n-1)$, ce qui fait que l'ordre de grandeur est
      toujours linéaire en $n$.
      En termes techniques, on écrirait $A'(n,T)=\Theta(n)$.
    \end{answer}
  \item Bonus: écrire une fonction en Python qui réalise cet algorithme.
    \begin{answer}
      On peut transcrire l'algorithme proposé plus haut de la façon suivante,
      en prenant bien garde à décaler les indices de $1$ puisqu'un tableau de
      longueur $n$ a en Python des éléments repérés par des indices de $0$ à
      $n-1$:
      \begin{lstlisting}[gobble=8]
        def tranche_minimale(T):
            g = d = k = 0
            m = f = T[0]
            for i in range(1, len(T)):
                if f >= 0:
                    f = T[i]
                    k = i
                else:
                    f += T[i]
                if f < m:
                    m = f
                    g = k
                    d = i
            return (g, d)
    \end{lstlisting}
    Mise à part la considération sur les indices, le seul ajustement
    consiste à remplacer la structure ``tant que $i < n$'' associée à un
    incrément de $i$ par une itération de $i$ dans l'intervalle $[1,n-1]$
    (la borne est modifiée du fait du décalage des indices).
    La forme avec ``tant que'' est un peu mieux adaptée au raisonnement par
    invariants, alors que la forme \lstinline{for i in range(...)} est plus
    concise et plus dans les habitudes en Python.
  \end{answer}
\end{enumerate}

\end{document}
