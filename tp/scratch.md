% Introduction à Scratch: TP
% M1 MEEF maths
% 4 octobre 2021

## Exercice 1: Tracé de carrés

1. Écrire un script pour réaliser la figure suivante, constituée de 5 carrés
   de côté 100, espacés de 20 unités.

```asy
unitsize(.5pt);
for (int x = 0; x <= 480; x += 120)
   draw(box((x-50,-50), (x+50,50)));
```

2. Écrire un script pour réaliser la figure suivante, constituée de 10 carrés
   concentriques de côtés variant de 20 à 200:

```asy
unitsize(.5pt);
for (int x = 10; x <= 100; x += 10)
   draw(box((-x,-x), (x,x)));
```

3. Transformer ce script en un bloc personnalisé pour pouvoir réaliser la
   frise suivante:

```asy
unitsize(.5pt);
pair d = (0,0);
for (int n = 100; n >= 10; n -= 10) {
   for (int x = 10; x <= n; x += 10)
      draw(shift(d) * box((-x,-x), (x,x)));
   d += (2 * n, 0);
}
```


## Exercice 2: Systèmes de Lindenmayer

Les systèmes de Lindenmayer, ou *L-systèmes*, sont des méthodes pour
représenter des figures fractales. L'idée est de partir d'une figure
géométrique simple (segment, triangle...) puis de remplacer chaque
segment par une certaine figure géométrique, et de répéter ce processus
un certain nombre de fois. Dans cet exercice, on s'intéresse à deux
exemples facilement réalisables en Scratch.

1. Le *flocon de von Koch* consiste à partir d'un triangle équilatéral et
   répéter la substitution suivante sur les segments:

```asy
unitsize(3cm);
draw((0,0) -- (1,0));
draw((1.5,0) -- (2,0), arrow=Arrow);
draw(shift(2.5,0) * ((0,0) -- (1/3,0) -- (1/2,sqrt(3)/6) -- (2/3,0) -- (1,0)));
```

   En partant d'un triangle équilatéral et en itérant trois fois cette
   substitution, en prenant soin de diviser la longueur des segments à chaque
   étape, on obtient la figure suivante:

```asy
import defs;
unitsize(4cm);
transform[] triangle = {
      identity,
      shift(1,0) * rotate(-120),
      shift(1/2,-sqrt(3)/2) * rotate(120)
   };
draw(iter(triangle, iter(kochT, (0,0) -- (1,0), 3)));
```

   Réaliser une telle figure en Scratch, de sorte que l'on puisse facilement
   changer le nombre d'itérations ou la taille de la figure.

2. Les L-systèmes ont été inventés pour modéliser la croissance de plantes et
   ils permettent de réaliser de belles figures de formes végétales. Par
   exemple, en itérant la règle suivante:

```asy
import defs;
unitsize(3cm);
path[] p = (0,0) -- (1,0);
draw(p);
draw((1.5,0) -- (2,0), arrow=Arrow);
draw(shift(2.5,0) * iter(plantT, p));
```

   on obtient au bout de 5 itérations la figure suivante:

```asy
import defs;
unitsize(5cm);
draw(iter(plantT, (0,0) -- (1,0), 5));
```

   Réaliser une telle figure.

3. Enrichir le script précédent pour ajouter une dose d'aléatoire dans les
   angles employés et faire varier l'épaisseur des traits.
