# Autour de l'algorithme d'Euclide

Le but de cette séance est d'étudier le grand classique des algorithmes
de l'arithmétique: [celui
d'Euclide](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide), qui
permet de calculer le PGCD de deux entiers. Autour de ce calcul, on va
faire de la programmation en Python et de l'algorithmique.

## 1. Le PGCD

L'algorithme se base sur le théorème d'existence du PGCD:

> **Théorème.** Pour tous entiers $a$ et $b$, il existe un entier $d$
> tel que
>
> -   $d$ divise $a$ et $b$,
> -   tout diviseur commun de $a$ et $b$ divise $d$.
>
> Cet entier est appelé PGCD (plus grand commun diviseur) de $a$ et $b$,
> on peut le noter $a\wedge b$.

Grandes lignes de la démonstration:

-   Sans perte de généralité, on suppose $a\geq b$.
-   On procède par récurrence (forte) sur $b$:
    -   Si $b=0$ alors $d=a$ convient.
    -   Si $b\neq 0$, on pose la division euclidienne $a=bq+r$, avec
        $0\leq r<b$.\
        Par hypothèse de récurrence, il existe $d$ qui divise $b$ et $r$
        et tel que tout diviseur commun de $b$ et $r$ divise $d$.\
        On vérifie que l'on a $d=a\wedge b$.

## 2. Élaboration de l'algorithme

La démonstration du théorème d'existence du PGCD donne un procédé de
calcul.

**Question 1.** En partant de la démonstration, expliciter les équations
qui définissent $a\wedge b$ par récurrence en fonction de $a$ et $b$.

**Question 2.** À partir de ces équations, définir une fonction Python
récursive `pgcd_rec` qui calcule le PGCD de deux entiers.

``` {.python .cell}
def pgcd_rec(a, b):
    ...
```

Le code suivant teste que la fonction ainsi définie donne le bon
résultat sur un série d'exemples pris au hasard, en utilisant la
fonction `math.gcd` fournie par Python pour vérifier. Modifiez votre
code jusqu'à ce que ça marche!

``` {.python .cell}
import math, random
def teste_pgcd(fonction, essais=1000):
    ok = True
    for i in range(essais):
        a = random.randint(0, 100000)
        b = random.randint(0, 100000)
        attendu = math.gcd(a, b)
        obtenu = fonction(a, b)
        if attendu != obtenu:
            print("Résultat incorrect pour a=%d et b=%d: %d au lieu de %d." % (a, b, obtenu, attendu))
            ok = False
    if ok:
        print("Résultats corrects pour %d essais." % essais)
teste_pgcd(pgcd_rec)
```

**Question 3.** Démontrer que l'algorithme ainsi implémenté est
correct, c'est-à-dire que la fonction renvoie toujours un résultat et
que c'est toujours celui qu'il faut.

L'algorithme récursif que vous avez implémenté est un cas de [récursion
terminale](https://fr.wikipedia.org/wiki/R%C3%A9cursion_terminale),
c'est-à-dire que dans le cas où la fonction se rappelle elle-même
récursivement, le résultat renvoyé est celui que renverra l'appel
récursif. Un algorithme de cette forme peut être transformé pour être
mis sous forme itérative, c'est-à-dire fondé sur une boucle.

**Question 4.** Implémenter une version itérative de l'algorithme
d'Euclide et justifier qu'elle est équivalente à la version récursive
précédente.

``` {.python .cell}
def pgcd_iter(a, b):
    ...
```

Tester cette fonction pour vérifier qu'elle fonctionne:

``` {.python .cell}
teste_pgcd(pgcd_iter)
```

**Question 5.** Démontrer que l'algorithme itératif est correct.

## 3. Étude de complexité

On rappelle la définition de la [suite de
Fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci): $$
    \begin{aligned}
        F_0 &= 0 \\
        F_1 &= 1 \\
        F_n &= F_{n-1} + F_{n-2} \qquad\text{pour } n\geq 2
    \end{aligned}
$$ La [formule de
Binet](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci#Expression_fonctionnelle)
en donne une expression close: $$
    F_n = \frac{1}{\sqrt5}\Bigl( \phi^n - (\phi')^n \Bigr)
    \qquad\text{où}\quad \phi = \frac{1+\sqrt5}{2}
    \quad\text{et}\quad \phi' = \frac{1-\sqrt5}{2} .
$$

**Question 6.** Déterminer le nombre d'itérations que fait
l'algorithme d'Euclide itératif si $a$ et $b$ sont deux termes
consécutifs de la suite de Fibonacci.

**Question 7.** En déduire une majoration du nombre d'itérations
utilisées pour le calcul de $a\wedge b$ en fonction de $a$ et $b$.

## 4. Calcul des coefficients de Bézout

L'[identité de
Bézout](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bachet-B%C3%A9zout)
affirme:

> **Théorème.** Pour tous entiers $a$ et $b$, il existe deux entiers $u$
> et $v$ tels que $a\wedge b=au+bv$.

La démonstration peut se faire en étendant le raisonnement utilisé pour
le théorème d'Euclide:

- Sans perte de généralité, on suppose $a\geq b$.
- On procède par récurrence (forte) sur $b$:
  - Si $b=0$ alors $a\wedge b=a$ et donc $a\wedge b=1a+0b$.
  - Si $b\neq 0$, on pose la division euclidienne $a=bq+r$, avec $0\leq r<b$.\
    Par hypothèse de récurrence, il existe $u'$ et $v'$ tel que $b\wedge r=u'b+v'r$.\
    Comme $a\wedge b=b\wedge r$ et $r=a-bq$, on en déduit $a\wedge b=v'a+(u'-v'q)$.

**Question 8.** Élaborer un algorithme itératif pour calculer les coefficients
de Bézout.

**Question 9.** Implémenter cet algorithme en Python, sous forme d'une
fonction `bezout(a,b)` qui renvoit un triple `(d,u,v)` où `d` est le PGCD de
`a` et `b` et où `u` et `v` sont les coefficients de Bézout associés.

``` {.python .cell}
def bezout(a, b):
    ...
```

Le code qui suit est un programme qui teste cette fonction, en utilisant
`math.gcd` pour vérifier que c'est bien le PGCD qui est calculé, et en
vérifiant la relation de Bézout.

``` {.python .cell}
essais = 1000
ok = True
for i in range(essais):
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    attendu = math.gcd(a, b)
    obtenu, u, v = bezout(a, b)
    if attendu != obtenu:
        print("Résultat incorrect pour a=%d et b=%d: %d au lieu de %d." % (a, b, obtenu, attendu))
        ok = False
    if a * u + b * v != obtenu:
        print("Relation non satisfaite: %d * %d + %d * %d != %d." % (a, u, b, v, obtenu))
        ok = False
if ok:
    print("Résultats corrects pour %d essais." % essais)
```

## 5. Généralisation

L'algorithme d'Euclide et sa version étendue qui donne les coefficients de
Bézout s'applique aussi aux polynômes. On propose donc de faire en sorte que
les fonctions précédentes s'appliquent à des polynômes.

On représente des polynômes par des listes coefficients, l'élément de rang $i$
étant le coefficient de $X^i$. Par exemple $3X^3+5X-7$ sera représenté par la
liste `[-7,0,5,3]`.

- Programmer l'addition, la soustraction et la multiplication de polynômes.
- Programmer la division euclidienne de polynômes.
- Programmer le calcul du PGCD de polynômes.
