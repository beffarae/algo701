% Scratch: états et événements
% M1 MEEF maths
% 15 novembre 2021

## Tout noir tout blanc

Le jeu dit *Tout noir tout blanc* est un jeu de type solitaire. Il se
joue sur une grille carrée de trois cases de côté où chaque case
contient un jeton qui a un côté noir et un côté blanc. Au départ, les
jetons sont orientés de façon aléatoire. À chaque coup, le joueur
choisit un jeton et le retourne ainsi que ses éventuels voisins situés et
haut, en bas, à gauche ou à droite (pas en diagonale). La partie est gagnée si
tous les jetons sont tournés avec le côté noir visible.

1.  Créer une scène Scratch correspondant au jeu, avec un lutin par
    jeton. On demande que cliquer sur le drapeau vert place les
    jetons dans une position aléatoire et que cliquer sur un jeton le
    retourne, sans affecter ses voisins pour le moment.

2.  Faire en sorte que le clic sur un jeton retourne aussi les jetons
    voisins, selon la règle. On utilisera pour cela les *messages* que
    fournit Scratch.

3.  Enrichir le programme pour détecter quand tous les jetons sont de la
    même couleur et afficher un message quand cela arrive.

4.  Déterminer une condition sur les configurations du jeu pour caractériser
    celles dans lesquelles il est possible de gagner.

5.  Ajouter au programme de quoi tester cette condition au démarrage, pour
    être sûr que le jeu ne commence que dans des configurations où on peut
    gagner.
