% Scratch: programmation événementielle
% M1 MEEF maths
% 11 octobre 2021

Cette page apporte quelques commentaires sur la résolution du
[sujet de TP](tp-evenementiel.html) sur la programmation événementielle en
Scratch, pour expliquer les scripts proposés comme correction.

## Mouvement simple

Pour avancer à vitesse constante, il suffit de faire une boucle « répéter
indéfiniment » et d'y mettre une instruction « avancer ». Pour le moment, on
fixe une vitesse constante en disant d'avancer de 5 pas.

Script: [déplacement simple](balles/simple.sb3).

## Rebond sur les bords

Pour rebondir sur les bords, il suffit d'ajouter l'instruction « rebondir si
le bord est atteint » à l'intérieur de la boucle, cela aura pour effet de
changer la direction si c'est nécessaire.

Script: [rebond sur les bords](balles/rebond.sb3).

## Pause

Pour réaliser un mécanisme de pause, il faut prendre en compte différents
éléments:

- L'événement qui alterne entre mouvement est pause est l'appui sur la touche
*espace*. Il y a dans la catégorie *contrôle* un en-tête « quand la touche
[espace] est pressée » qui permet de réagir à cet événement. On pourra
remarquer qu'il y a aussi une condition « touche [espace] pressée? » dans la
catégorie *capteurs*, mais celle-ci ne convient par parce qu'elle fait
référence à l'*état* de la touche au moment où la condition est testée et pas
à l'événement de l'appui, qui constitue un *changement d'état*. On ne peut
donc pas utiliser de test sur cette condition dans la boucle de déplacement.

* Pour que la boucle de déplacement puisse être affectée par l'appui sur une
touche, il faut donc utiliser une variable. On peut utiliser une variable
*pause* qui contiendra 1 si on est en pause et 0 sinon. Alors la boucle de
déplacement de fera effectivement un mouvement que si cette variable vaut 0,
ce qui peut se réaliser avec une simple condition « si … alors … ».

- Il est nécessaire que les deux balles se mettent en pause et qu'elles
reprennent leur mouvement en même temps. Pour cela on s'assure que la variable
*pause* est partagée entre tous les lutins et on inclut la condition sur cette
variable dans le programme de chaque lutin. Attention cependant à ne traiter
l'événement d'appui sur une touche que dans un seul des deux lutins, sans quoi
à chaque appui sur *espace*, chaque lutin inverserait la valeur de *pause* et
les deux changements s'annuleraient.

- On prend soin de mettre *pause* à 0 en début de programme, quand le drapeu
est cliqué.

Scirpt: [pause](balles/pause.sb3).

Un petit défaut de cette solution est que les scripts continuent à tourner
sans rien faire tant qu'on est en pause. On peut améliorer cela en faisant en
sorte que la boucle de déplacement s'arrête quand on est en pause, en
utilisant un « répéter jusqu'à ». Il faut alors voir comment faire reprendre
le mouvement lors du prochain appui. On ne peut pas mettre le mouvement
lui-même dans le bloc qui traite l'appui sur la touche *espace* mais on peut
créer un événement spécialement pour déclencher le mouvement, au moyen des
*messages*: on crée un message (appelons-le *action*) et on fait en sorte que
les lutins se déplacent quand ils reçoivent ce message. Le bloc qui traite
l'appui sur *espace* envoie le message *action* à tous les lutins quand il
détecte qu'on n'est plus en pause. Le bloc qui traite l'appui sur le drapeau
vert se contente alors de mettre *pause* à 0 et d'envoyer le message pour
commencer le mouvement.

Script: [pause avec message](balles/pause-message.sb3).

## Rebond sur l'autre balle

Pour réaliser le rebond sur l'autre balle, il y a deux choses à faire: d'une
part calculer un changement de direction, d'autre part traiter au bon moment
le choc en tant qu'événement au sein du programme.

### Calcul du rebond

Le but n'est pas de faire une simulation précise d'un problème de mécanique,
on admet donc des règles simples pour le besoin de l'exercice: d'une part la
direction après le choc est symétrique de la direction avant le choc par
rapport à l'axe tangent au choc, celui-ci étant perpendiculaire à la droite
qui relie les centres des deux balles (parce que celles-ci sont supposées
rondes), d'autre part les vitesses sont préservées (ce qui est raisonnable si
les deux balles ont la même masse et la même vitesse avant le choc).

Il y a donc à calculer l'angle de déplacement après le choc, qui ne dépendra
que de l'angle de déplacement avant le choc (la direction du lutin, notons-la
$d$) et de la direction du choc (direction vers l'autre lutin, notons-la $c$).
L'angle entre le choc et le déplacement est donc $d-c$ et le symétrique de
la direction $d$ par rapport à la tangente au choc s'exprime donc (en degrés)
comme $c+180-(d-c)=180+2c-d$.

La valeur de $d$ est obtenue à partir du bloc « direction » dans la catégorie
*mouvement*. Pour la valeur de $c$, la seule possibilité est d'utiliser
« s'orienter vers… » dans la catégorie *mouvement* puis utiliser à nouveau le
bloc « direction ». Il faut donc introduire une variable pour retenir la
direction avant le choc.

On peut mettre cela en œuvre en ajoutant aux scripts précédents, dans la boucle
de déplacement, un test sur la condition « touche le … ? » disponible dans
la catégorie *capteurs*, pour réaliser le changement de direction en cas de
contact. On inclut ce test dans le script de chacune des deux balles.

Script: [choc avec test de contact](balles/choc-test.sb3).

### Le choc comme événement

Le comportement du script précédent ne correspond pas à ce que l'on
attendrait car on voit apparaître deux phénomènes étranges:

- Parfois, les directions après rebond ne semblent pas correspondre aux règles
que nous avons suivies, parfois il semble même qu'une seule des deux balles
rebondisse alors que l'autre continue sa course sans changer de direction.
Cela s'explique par le fait qu'une balle peut détecter le contact, changer de
direction puis avancer dans sa nouvelle direction avant que l'autre n'ait eu
l'occasion de traiter le contact. Alors la deuxième balle peut détecter le
contact après mouvement de la première et donc le voir dans une direction
différente, voire ne pas détecter le contact du tout si la première balle
s'est éloignée.

- Parfois il semble que les deux balles restent collées l'une à l'autre en
changeant continuellement de direction pendant un certain temps, avant de
repartir. Ce phénomène s'explique par le fait qu'une balle peut encore
détecter le contact avec l'autre après avoir rebondi si les deux balles ne se
sont pas encore éloignées assez après rebond. Le programme se comportera alors
comme s'il y avait un deuxième rebond, alors que c'est le même contact qui n'a
pas encore été traité complètement.

Pur régler ces problèmes, il faut donc s'assurer que le choc est traité par
les deux balles en même temps et une seule fois par choc,  c'est-à-dire
détecter et traiter l'événement qui correpsond à l'instant où les balles
entrent en contact, donc où le test « touche le … ? » passe de *faux*
à *vrai*.

*À suivre…*
