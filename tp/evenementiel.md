% Scratch: programmation événementielle
% M1 MEEF maths
% 11 octobre 2021

Le but de cette séance est de comprendre les bases de la programmation dite
[événementielle](https://fr.wikipedia.org/wiki/Programmation_%C3%A9v%C3%A9nementielle)
au moyen d'une activité basée sur Scratch. Les notions en jeu sont les
suivantes:

événement
:   C'est une condition qui peut se produire à un moment au cours de
    l'exécution du programme, sans que l'on sache a priori quand. Cela
    peut être quelque chose d'extérieur au programme (l'utilisateur
    appuie sur une touche, un délai est écoulé, etc) ou quelque chose
    d'interne (deux variables sont égales, une expression prend une
    certaine valeur).

gestionnaire d'événement
:   C'est la partie du programme qui définit comment on va réagir à un
    événement donné. En Scratch, selon, le type d'événement, cela peut
    correspondre à un bloc indépendant avec un en-tête de la forme
    « Quand... » (en jaune, dans la catégorie *Événements*) ou à une
    structure « Si...alors... » à l'intérieur d'une boucle. La catégorie
    *Capteur* permet de détecter certains événements dans ce cas.

portée de variable
:   Les variables peuvent être globales à tout le système ou exister
    pour un lutin indépendemment des autres. La première forme permet de
    communiquer entre lutins, la seconde permet de faire des traitements
    spécifiques à chaque lutin sans interférence.

## Déplacements et rebonds

1.  Créer deux lutins en forme de balles et faire en sorte que, lorsque
    le drapeau vert est cliqué, ces deux lutins se mettent à avancer en
    ligne droite, à vitesse constante, dans leur direction courante.

2.  Faire en sorte que les balles rebondissent quand elles arrivent sur un
    bord de la scène.

    Il y a dans *Mouvement* une action « rebondir si le bord est atteint » qui
    change la direction comme il faut.

3.  Faire en sorte que la touche *espace* se comporte comme un bouton
    *pause*: un appui immobilise la scène, un autre appui la fait
    reprendre.

    Il faut que la boucle qui réalise le mouvement tiennent compte de l'état
    courant du système: soit en cours d'exécution, soit en pause.

4.  Faire en sorte que les balles rebondissent l'une sur l'autre quand elles
    se touchent, de façon physiquement crédible (on pourra considérer qu'il
    s'agit d'un [choc élastique] et que les balles ont même masse, par
    exemple).

    Cela implique de calculer la direction du mouvement après le choc en
    fonction de la direction avant le choc et de la direction dans laquelle se
    trouve l'autre balle. On peut utiliser pour cela l'expression
    « direction » en bleu (dans *Mouvement*, donne l'angle de déplacement du
    lutin qui exécute le scirpt) et « s'orienter vers… » pour connaître la
    direction de l'autre balle. On pourra avoir besoin d'introduire des
    variables pour faire le calcul.

    [choc élastique]: https://fr.wikipedia.org/wiki/Choc_%C3%A9lastique

    ```asy
    pair p1 = (0,0), d1 = (2cm, -1cm);
    pair p2 = (3cm,1cm);
    fill(circle(p1, 2pt));
    fill(circle(p2, 2pt));
    pair contact = p2 - p1, t = contact * (0, 1);
    draw(circle(p1, length(p2 - p1)/2));
    draw(circle(p2, length(p2 - p1)/2));
    pen dashed = linetype(new real[] {8,8});
    draw(p1 - contact -- p2 + contact, dashed);
    draw(p1 - t -- p1 + t, dashed);
    pair after = d1 * expi(pi + 2 * (angle(contact) - angle(d1)));
    draw(p1 -- p1 + d1, arrow=EndArrow);
    label("$v$", p1 + d1/2, unit(d1) * (0,1));
    draw(p1 -- p1 + after, arrow=EndArrow);
    label("$v'$", p1 + after/2, unit(after) * (0,1));
    ```

    Sur le dessin, $v$ est le vecteur vitesse avant le choc, $v'$ est le
    vecteur vitesse après le choc, symétrique par rapport à la tangente à la
    direction de l'impact (si on suppose que les deux balles ont la même
    vitesse et la même masse).

5.  Ajouter une composante d'accélération verticale dirigée vers le bas,
    pour simuler de la gravitation.

    Il faut doc faire évoluer la direction du déplacement et la vitesse. Il
    faudra aussi tenir compte de cette modificaiton dans le calcul des chocs.

6.  Faire en sorte que lorsque l'utilisateur appuie sur la touche B, une
    nouvelle balle soit créée, soumise aux mêmes lois que les autres,
    avec une position et une direction initiales aléatoires.
