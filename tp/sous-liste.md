% Plus longue sous-liste commune
% M1 MEEF maths
% 6 décembre 2021

## Définitions

Par *liste*, on entend une suite finie de valeurs, que l'on numérotera
à partir de $1$.
On notera les listes avec des lettres majuscules et les éléments avec des
minuscules, par exemple $X=(x_1,x_2,\ldots,x_n)$.
On appelle *longueur* le nombre d'éléments de la liste.

Une *sous-liste* de $X$ est une liste obtenue à partir de $X$ en retirant
certains éléments (éventuellement aucun, éventuellement tous) et en gardant
dans le même ordre ceux qui restent.
Par exemple, si $X=(8,2,9,7,3,2,6)$, alors $(8,3,6)$, $(2,9,7,3,2)$ et $(3)$
sont des sous-listes de $X$, de même que $()$ et $X$, mais $(7,9)$ et
$(7,3,3,2)$ n'en sont pas.

Dans la suite, on ne précise pas la nature des éléments des listes, cela n'a
pas d'importance.
Tout ce qui compte est que l'on peut déterminer si deux éléments sont égaux.

## Le problème

On dispose de deux listes $A$ et $B$ et on cherche une liste $C$ qui soit une
sous-liste à la fois de $A$ et de $B$ et qui soit de longueur maximale.
Dans la suite on notera toujours $m$, $n$ et $k$ les longueurs respectives de
$A$, $B$ et $C$.


1.  Déterminer toutes les sous-listes de longueur maximale dans les cas
    suivants:

    - $A=(0,1,0,0,1)$ et $B=(0,0,1,1)$
    - $A=(s,t,u,t,v,s,t)$ et $B=(t,v,u,s,t,s)$

2.  Montrer que pour toutes listes $A$ et $B$, il y a toujours au moins une
    sous-liste commune de longueur maximale.

## Première approche

On commence par étudier un algorithme naïf:

> | $C := ()$.
> | Pour chaque sous-liste $X$ de $A$,
> |     pour chaque sous-liste $Y$ de $B$,
> |         si $X = Y$ et ${|X|}>{|C|}$, faire $C := X$.
> | Renvoyer $C$.

On cherche à évaluer son efficacité.

3.  En notant $m$ et $n$ les longueurs respectives des listes $A$ et $B$,
    déterminer le nombre total de sous-listes de $A$ et le nombre total de
    sous-listes de $B$.

4.  En déduire le nombre de tests que fait cet algorithme.

## Deuxième approche

On va maintenant construire un algorithme plus intelligent pour déterminer
une sous-liste commune maximale.

5.  Que peut-on dire si $A$ ou $B$ est vide ?

6.  On suppose que les listes $A$ et $B$ ne sont pas vides et qu'elles ont une
    sous-liste commune maximale $C$ non vide.
    On note $A'$, $B'$ et $C'$ les listes $A$, $B$ et $C$ auxquelles on a
    retiré le dernier élément (elles sont donc de longueurs respectives
    $m-1$, $n-1$ et $k-1$). Montrer que

    - si $a_m=b_n$ alors $c_k=a_m$ et $C'$ est une sous-liste maximale
      de $A'$ et $B'$,

    - si $a_m\neq b_n$ et $c_k\neq a_m$ alors $C$ est une sous-liste
      maximale de $A'$ et $B$,

    - si $a_m\neq b_n$ et $c_k\neq b_n$ alors $C$ est une sous-liste
      maximale de $A$ et $B'$.

7.  En déduire que

    - pour déterminer une sous-liste maximale de $A$ et $B$ quand
      $a_m=b_n$ il suffit de déterminer une sous-liste maximale de $A'$ et
      $B'$,

    - pour déterminer une sous-liste maximale de $A$ et $B$ quand
      $a_m\neq b_n$, il suffit de déterminer une sous-liste maximale pour
      $A'$ et $B$ et une pour $A$ et $B'$ et de garder la plus longue des
      deux.

8.  Écrire un algorithme récursif qui détermine une sous-liste maximale, en
    utilisant les remarques précédentes.

## Efficacité de l'algorithme

On cherche maintenant à déterminer l'efficacité de ce nouvel algorithme.

9.  Étant données deux listes $A$ et $B$, déterminer l'ensemble des
    sous-listes de $A$ et de $B$ qui sont susceptibles d'apparaître dans des
    appels récursifs de l'algorithme précécdent.

10. En déduire une majoration du nombre d'appels récursifs *différents* qui
    peuvent avoir lieu.

Cette majoration donne l'efficacité de l'algorithme récursif, à condition que
l'on prenne soin de ne pas faire des calculs intermédiaires plusieurs fois.
On peut faire cela en créant un tableau qui recevra tous les résultats
intermédiaires et en le remplissant dans un ordre tel que chaque
fois qu'on a besoin d'un résultat pour un sous-problème, celui-ci se trouve
déjà dans le tableau.
Cette technique s'appelle la *programmation dynamique*.
