% Autour du rendu de monnaie
% M1 MEEF maths
% 7 décembre 2020

*Cette page contient les notes tapées en partage d'écran pendant la séance,
pour remplacer un tableau. Donc c'est brouillon, mais c'est normal.
Reportez-vous au [sujet](tp-monnaie.html) pour retrouver l'énoncé des
questions.*


## Question 1

Décomposition: 28€43 = 20€ + 5€ + 2€ + 1€ + 20c + 20c + 2c + 1c

Optimalité: quelque chose à voir avec les diviseurs de 10 ?
À préciser dans la suite.

## Question 2

En prenant les plus grandes pièces disponibles chaque fois: 49 = 30 + 12 + 6 + 1

Décomposition optimale: 49p = 24 + 24 + 1

Optimalité: il n'y a pas de somme de deux pièces qui fasse 49

## Question 3

On peut décomposer toutes les valeurs de v
si et seulement si
la plus petite pièce vaut 1 (donc s[1]=1).

Justification:
- Si on a 1, on peut faire toutes les sommes;
- si on n'a pas 1, on ne peut pas faire 1.

## Question 4 : algorithme glouton

Entrée:

- le système S = (s[1], ..., s[n])
- la valeur v

Sortie:

- une décomposition D = (d[1], ..., d[n])

Première proposition:

    i := n
    tant que i > 0:
        d[i] := 0
        tant que v ≥ s[i]:
            v := v - s[i]
            d[i] := d[i] + 1
        i := i - 1
    renvoyer D

Autre proposition:

    i := n
    r := v
    tant que i ≠ 0:
        d[i] := r // s[i]     (division entière)
        r := r - d[i] * s[i]  (reste de la division)
        i := i - 1

C'est équivalent sauf que le premier décompose explicitement la division
euclidienne.

Autre proposition:

    r := v
    pour chaque i de n à 1 (par pas de -1):
        d[i] := r // s[i]     (division entière)
        r := r - d[i] * s[i]  (reste de la division)

## Question 5 : terminaison

Toutes les boucles sont basées sur la valeur d'une variable entière qui sera
strictement décroissante, avec une condition d'arrêt qui impose qu'elle reste
positive, donc il ne peut y avoir qu'un nombre fini d'itérations.

## Question 6 : correction

On cherche un invariant de boucle

    r := v
    pour chaque i de n à 1 (par pas de -1):
      { invariant de boucle ici }
        d[i] := r // s[i]     (division entière)
        r := r - d[i] * s[i]  (reste de la division)

Invariant: fait intervenir D, r, i et aussi v et S

  r ≥ s[i]  →  pas toujours vrai

  r + ∑(j>i) d[j] s[j] = v

### Au début de 1ère itération:

| on a i = n donc on n'a aucun j tel que j>n
| donc ∑(j>i) d[j] s[j] = 0
| or r = v
| donc c'est bon

### Étape de récurrence:

| on suppose que l'invariant est satisfait au début de l'itération numéro i
| on veut montrer qu'il l'est au début de l'itération i - 1
|
| on note r' et d' les valeurs des variables à la fin de l'itération numéro i
|
| d'[j] = d[j] pour chaque j ≠ i
| d'[i] = r // s[i]
| r' = r - d'[i] s[i]
|
| on suppose:   r + Σ(j>i) d[j] s[j] = v
| on veut:      r' + ∑(j>i-1) d'[j] s[j] = v
| c'est-à-dire: r' + ∑(j≥i) d'[j] s[j] = v
|
| r' + ∑(j≥i) d'[j] s[j]
| = r - d'[i] s[i] + ∑(j≥i) d'[j] s[j]
| = r - d'[i] s[i] + d'[i] s[i] + ∑(j>i) d'[j] s[j]
| = r + ∑(j>i) d'[j] s[j]
| = r + ∑(j>i) d[j] s[j]
| = v

### En sortie de boucle:

  à la fin de la dernière itération, l'invariant est satisfait pour i = 0

  r + ∑ d[j] s[j] = v

Par ailleurs on a par hypothèse s[1] = 1
donc lors de l'itération i = 1, on a un reste nul (division par 1)
donc en fin d'itération on a r = 0.

Donc en fin d'algorithme on a ∑ d[j] s[j] = v

## Question 7

On a un contre-exemple dans la question 2.

## Question 8

Si v est impair, on doit utiliser le 1 au moins une fois.
Si on utilise le 1 au moins 2 fois, alors en remplaçant 1+1 par 2, on obtient
une meilleure décomposition, donc dans l'optimal on utilise 1 une fois
exactement.

Plus généralement: chaque pièce (sauf la plus grande) est utilisée au plus une
fois dans une décomposition optimale.

Si v est pair, une décomposition optimale n'utilise pas 1, donc seules des
pièces multiples de 2 sont utilisées.
Décomposer v dans le système (2,4,8,…,2^m)
revient à décomposer v/2 dans le système (1,2,4,…,2^(m-1))

Si on sait que l'algorithme glouton est optimal pour v/2 dans (1,2,4,…,2^(m-1)),
le même algorithme pour v dans (1,2,4,…,2^m) se comporte pareil,
donc la solution donnée pour v pair sera optimale.

*Demandez-moi si vous voulez des détails pour avoir une solution plus
rédigée…*
