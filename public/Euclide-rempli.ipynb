{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Autour de l'algorithme d'Euclide — *correction*\n",
    "\n",
    "Le but de cette séance est d'étudier le grand classique des algorithmes de l'arithmétique: [celui d'Euclide](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide), qui permet de calculer le PGCD de deux entiers. Autour de ce calcul, on va faire de la programmation en Python et de l'algorithmique.\n",
    "\n",
    "## 1. Le PGCD\n",
    "\n",
    "L'algorithme se base sur le théorème d'existence du PGCD:\n",
    "\n",
    "> **Théorème.**\n",
    "> Pour tous entiers $a$ et $b$, il existe un entier $d$ tel que\n",
    "> - $d$ divise $a$ et $b$,\n",
    "> - tout diviseur commun de $a$ et $b$ divise $d$.\n",
    "> \n",
    "> Cet entier est appelé PGCD (plus grand commun diviseur) de $a$ et $b$, on peut le noter $a∧b$.\n",
    "\n",
    "Grandes lignes de la démonstration:\n",
    "\n",
    "- Sans perte de généralité, on suppose $a≥b$.\n",
    "- On procède par récurrence (forte) sur $b$:\n",
    "  - Si $b=0$ alors $d=a$ convient.\n",
    "  - Si $b≠0$, on pose la division euclidienne $a=bq+r$, avec $0≤r<b$.  \n",
    "    Par hypothèse de récurrence, il existe $d$ qui divise $b$ et $r$ et tel que tout diviseur commun de $b$ et $r$ divise $d$.  \n",
    "    On vérifie que l'on a $d=a∧b$.\n",
    "\n",
    "## 2. Élaboration de l'algorithme\n",
    "\n",
    "La démonstration du théorème d'existence du PGCD donne un procédé de calcul.\n",
    "\n",
    "**Question 1.** En partant de la démonstration, expliciter les équations qui définissent $a∧b$ par récurrence en fonction de $a$ et $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Les équations sont simplement\n",
    "\\begin{align*}\n",
    "    a\\wedge 0 &= a \\\\\n",
    "    a\\wedge b &= b \\wedge (a\\%b) &&\\text{si } b\\neq 0\n",
    "\\end{align*}\n",
    "où $a\\%b$ représente le reste de la division euclidienne de $a$ par $b$ (selon la notation de Python)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2.** À partir de ces équations, définir une fonction Python récursive `pgcd_rec` qui calcule le PGCD de deux entiers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> On traduit les deux équations en deux cas selon la condition `b == 0`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pgcd_rec(a, b):\n",
    "    if b == 0:\n",
    "        return a\n",
    "    else:\n",
    "        return pgcd_rec(b, a % b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le code suivant teste que la fonction ainsi définie donne le bon résultat sur un série d'exemples pris au hasard, en utilisant la fonction `math.gcd` fournie par Python pour vérifier. Modifiez votre code jusqu'à ce que ça marche!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Résultats corrects pour 1000 essais.\n"
     ]
    }
   ],
   "source": [
    "import math, random\n",
    "def teste_pgcd(fonction, essais=1000):\n",
    "    ok = True\n",
    "    for i in range(essais):\n",
    "        a = random.randint(0, 100000)\n",
    "        b = random.randint(0, 100000)\n",
    "        attendu = math.gcd(a, b)\n",
    "        obtenu = fonction(a, b)\n",
    "        if attendu != obtenu:\n",
    "            print(\"Résultat incorrect pour a=%d et b=%d: %d au lieu de %d.\" % (a, b, obtenu, attendu))\n",
    "            ok = False\n",
    "    if ok:\n",
    "        print(\"Résultats corrects pour %d essais.\" % essais)\n",
    "teste_pgcd(pgcd_rec)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.** Démontrer que l'algorithme ainsi implémenté est correct, c'est-à-dire que la fonction renvoie toujours un résultat et que c'est toujours celui qu'il faut."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> On démontre cela par récurrence forte sur $b$. Précisément, on appelle $P(b)$ la propriété\n",
    ">\n",
    "> > Pour tout entier naturel $a$, l'appel `pgcd_rec(a,b)` termine et renvoie la valeur $a\\wedge b$.\n",
    ">\n",
    "> Le cas initial $b=0$ est donc\n",
    "« Pour tout entier naturel $a$, l'appel `pgcd_rec(a,0)` termine et renvoie la valeur $a\\wedge 0$. »\n",
    "Le fait que l'appel termine vient du fait que la condition `b==0` est vraie et que l'on passe donc dans la première branche, qui renvoie directement une valeur. La valeur est `a`, ce qui est correct selon l'équation $a\\wedge0=a$.\n",
    ">\n",
    "> Pour l'étape de récurrence, on pose un $b$ quelconque non nul et on suppose $P(n)$ vraie pour tout $n<b$.\n",
    "Dans ce cas la condition `b==0` est fausse donc l'évaluation consiste à appeler `pgcd_rec(b,a%b)`.\n",
    "Par définition du reste de la division euclidienne, on a $0\\leq a\\%b<b$ donc l'hypothèse de récurrence s'applique à $a\\%b$, autrement dit $P(a\\%b)$ est vraie: pour tout entier $k$, l'appel `pgcd_rec(k,a%b)` termine et renvoie $k\\wedge(a\\%b)$. En particulier, pour $k=b$, on a que `pgcd_rec(b,a%b)` termine et renvoie $b\\wedge(a\\%b)$.\n",
    "On en déduit que `pgcd_rec(a,b)` termine et renvoie cette valeur.\n",
    "D'après l'équation $a\\wedge b=b\\wedge(a\\%b)$, la valeur renvoyée est bien $a\\wedge b$, ce qui conclut la démonstration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'algorithme récursif que vous avez implémenté est un cas de [récursion terminale](https://fr.wikipedia.org/wiki/R%C3%A9cursion_terminale), c'est-à-dire que dans le cas où la fonction se rappelle elle-même récursivement, le résultat renvoyé est celui que renverra l'appel récursif. Un algorithme de cette forme peut être transformé pour être mis sous forme itérative, c'est-à-dire fondé sur une boucle.\n",
    "\n",
    "**Question 4.** Implémenter une version itérative de l'algorithme d'Euclide et justifier qu'elle est équivalente à la version récursive précédente."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Le principe est de remarquer que l'appel récursif `return pgcd_rec(b,a%b)` consiste en fait à modifier les valeurs de `a` et `b` et à recommencer la fonction depuis le début. Faire cela directement conduit au code suivant construit sur une boucle sans fin:\n",
    ">\n",
    "> ```python\n",
    "> while True:\n",
    ">     if b == 0:\n",
    ">         return a\n",
    ">     else:\n",
    ">         r = a % b\n",
    ">         a = b\n",
    ">         b = r\n",
    "> ```\n",
    ">\n",
    "> Cette boucle sans fin ne peut être interrompue qu'avec l'instruction `return` qui termine la fonction. On peut reformuler cela en remarquant que `b == 0` joue le rôle de condition d'arrêt, autrement dit la boucle continue tant que cette condition est fausse:\n",
    ">\n",
    "> ```python\n",
    "> while b != 0:\n",
    ">     r = a % b\n",
    ">     a = b\n",
    ">     b = r\n",
    "> return a\n",
    "> ```\n",
    ">\n",
    "> Par ailleurs, la suite de trois affectations peut se simplifier grâce à une fonctionnalité de Python qui permet de décomposer un couple lors d'une affectation (voir par exemple [ce cours](https://courspython.com/tuple.html) sur les *tuples* en Python). En fin de compte, on obtient ceci:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pgcd_iter(a, b):\n",
    "    while b != 0:\n",
    "        a, b = b, a % b\n",
    "    return a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tester cette fonction pour vérifier qu'elle fonctionne:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Résultats corrects pour 1000 essais.\n"
     ]
    }
   ],
   "source": [
    "teste_pgcd(pgcd_iter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5.** Démontrer que l'algorithme itératif est correct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Pour démontrer qu'un algorithme itératif est correct, on décompose en général le raisonnement en deux parties:\n",
    ">\n",
    "> - correction partielle: on montre que si la boucle s'arrête, alors le résultat obtenu sera le bon, en général en montrant qu'un [invariant de boucle](https://fr.wikipedia.org/wiki/Invariant_de_boucle) a été préservé,\n",
    "> - [terminaison](https://fr.wikipedia.org/wiki/Terminaison_d%27un_algorithme): on montre que l'itération s'arrête toujours, en général en montrant qu'une certaine quantité décroît strictement d'une itération à la suivante.\n",
    ">\n",
    "> Appelons $a_0$ et $b_0$ les valeurs de $a$ et $b$ données en entrée.\n",
    ">\n",
    "> - Pour la correction partielle, on démontre que l'invariant $a\\wedge b=a_0\\wedge b_0$ est préservé.\n",
    ">   Cette propriété est vraie avant l'entrée dans la boucle, puisqu'on a alors $a_0=a$ et $b_0=b$ par définition.\n",
    ">\n",
    ">   Au début d'une itération, à cause de la condition de la boucle, on sait qu'on a $b\\neq 0$. En notant $a'$ et $b'$ les valeurs de $a$ et $b$ en fin d'itération, on a par construction $a'=b$ et $b'=a\\%b$, donc on a $a'\\wedge b'=a\\wedge b$ d'après les équations établies au dessus. Si on suppose l'invariant satisfait au début de l'itération, on a $a\\wedge b=a_0\\wedge b_0$, donc $a'\\wedge b'=a_0\\wedge b_0$ et l'invariant est satisfait en fin d'itération.\n",
    ">\n",
    ">   On a donc établi (par récurrence) que l'invariant est satisfait à la fin de chaque itération.\n",
    ">   Si la boucle termine, l'invariant est donc satsifait en sortie de boucle: $a\\wedge b=a_0\\wedge b_0$.\n",
    ">   Par ailleurs, d'après la condition de la boucle, si elle termine c'est qu'on $b=0$.\n",
    ">   On sait qu'on a alors $a\\wedge b=a\\wedge 0=a$, d'où $a_0\\wedge b_0=a$.\n",
    ">   On en déduit que la valeur renvoyée (à supposer que la boucle termine) est bien $a_0\\wedge b_0$.\n",
    "> \n",
    "> - Pour la terminaison, il suffit de remarquer qu'au sein de chaque itération, en reprenant les notations précédentes, on a $b'=a\\%b<b$, donc la suite des valeurs successives de $b$ est strictement décroissante.\n",
    ">   Comme cette suite est à valeur dans l'ensemble $\\mathbb{N}$ des entiers naturels, on en déduit qu'elle est forcément finie, donc la boucle doit terminer.\n",
    ">\n",
    "> On pourra remarquer que les arguments sont finalement très similaires au cas récursif: ce sont les équations établies au départ qui justifient la correction, par ailleurs la terminaison est obtenue par décroissance stricte de $b$, ce qui correspond au raisonnement par récurrence forte sur $b$ dans le cas récursif."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Étude de complexité\n",
    "\n",
    "On rappelle la définition de la [suite de Fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci):\n",
    "$$\n",
    "    \\begin{align*}\n",
    "        F_0 &= 0 \\\\\n",
    "        F_1 &= 1 \\\\\n",
    "        F_n &= F_{n-1} + F_{n-2} \\qquad\\text{pour } n≥2\n",
    "    \\end{align*}\n",
    "$$\n",
    "La [formule de Binet](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci#Expression_fonctionnelle) en donne une expression close:\n",
    "$$\n",
    "    F_n = \\frac{1}{\\sqrt5}\\Bigl( φ^n - (φ')^n \\Bigr)\n",
    "    \\qquad\\text{où}\\quad φ = \\frac{1+\\sqrt5}{2}\n",
    "    \\quad\\text{et}\\quad φ' = \\frac{1-\\sqrt5}{2} .\n",
    "$$\n",
    "\n",
    "**Question 6.** Déterminer le nombre d'itérations que fait l'algorithme d'Euclide itératif si $a$ et $b$ sont deux termes consécutifs de la suite de Fibonacci."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> En faisant tourner l'algorithme à la main des exemples tirés de la suite de Fibonacci $0,1,1,2,3,5,8,13,21,34\\dots$, on constate que si une itération commence avec deux termes consécutifs de la suite (par exemple 21 et 34), alors l'itération suivante commence avec deux termes consécutifs pris un rang avant (dans l'exemple, 13 et 21, puis 8 et 13, et ainsi de suite). On peut démontrer cette propriété.\n",
    ">\n",
    "> On commence par établir que, pour tout $n\\geq 2$, on a $F_{n+1}>F_n>0$.\n",
    "> Ceci peut se démontrer par récurrence: pour le cas initial il suffit d'observer que $F_2=1$ et $F_3=2$ donc la propriété est vraie.\n",
    "> Pour l'étape de récurrence, on a $F_{n+2}=F_{n+1}+F_n$ or $F_n>0$ par hypothèse de récurrence donc $F_{n+2}>F_{n+1}>0$.\n",
    "> La suite est donc strictement positive et croissante à partir du rang 2.\n",
    ">\n",
    "> On remarque ensuite que, sous l'hypothèse $F_{n+1}>F_n>0$, l'écriture $F_{n+2}=F_{n+1}+F_n$ est en fait un cas de division euclidienne $F_{n+2}=F_{n+1}\\times1+F_n$: la division de $F_{n+2}$ par $F_{n+1}$ a pour quotient $1$ et pour reste $F_n$.\n",
    "> Par conséquent, si une itération commence avec $(a,b)=(F_{n+2},F_{n+1})$ pour $n\\geq2$, l'itération suivante aura $(a,b)=(F_{n+1},F_n)$.\n",
    ">\n",
    "> Ce raisonnement s'applique jusqu'à tomber sur $(a,b)=(F_2,F_1)$ soit $(a,b)=(2,1)$, cas où les conditions ne sont plus remplies. Dans ce cas on a une itération, le quotient de $a$ par $b$ est $2$ et le reste est $0$, on se retrouve en fin d'itération avec $(a,b)=(1,0)$ et la boucle termine puis on renvoit la valeur $1$.\n",
    ">\n",
    "> De ces observations, on peut déduire le nombre d'itérations: si on part de $(a,b)=(F_{n+1},F_n)$ avec $n\\geq2$, en $k$ itérations on obtient $(a,b)=(F_{n+1-k},F_{n-k})$, donc en $n-1$ itérations on obtient $(a,b)=(F_2,F_1)$. Il reste une itération avant de terminer, on a donc exactement $n$ itérations pour $(a,b)=(F_{n+1},F_n)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 7.** En déduire une majoration du nombre d'itérations utilisées pour le calcul de $a∧b$ en fonction de $a$ et $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Intuitivement, l'idée est que dans le cas de termes consécutifs de la suite de Fibonacci, dans les divisions euclidiennes, tous les quotients sont égaux à 1 (hormis à la toute dernière étape), ce qui est la plus petite valeur possible, et qu'en conséquence la suite des valeurs de $a$ décroît le plus lentement possible dans ce cas. En quelque sorte, le cas étudié dans la question précédente est le *pire cas*.\n",
    ">\n",
    "> Formellement, on commence par établir que\n",
    ">\n",
    "> > Pour tous entiers naturels $a$, $b$ et $k$ tels que $a\\geq b$ et que l'algorithme d'Euclide appliqué à $(a,b)$ fait $k$ itérations, on a $a\\geq F_k$.\n",
    ">\n",
    "> On démontre cette propriété par récurrence forte sur $k$.\n",
    ">\n",
    "> - On traite à part deux cas initiaux: $k=0$ et $k=1$.\n",
    ">   Pour $k=0$, comme $F_0=0$, il n'y a rien à démontrer.\n",
    ">   Pour $k=1$, comme il y a une itération, on a $b\\neq0$ donc $a\\geq b\\geq 1$ or $F_1=1$ donc on a bien $a\\geq F_1$.\n",
    "> - Pour l'étape de récurrence, soit un entier $k\\geq2$, supposons le résultat acquis pour toutes les valeurs jusqu'à $k-1$. Comme $k\\geq 2$, on peut suivre les deux premières itérations.\n",
    ">   Dans la première, on a $b>0$ et on aboutit au couple $(b,a\\%b)$, avec $b\\geq a\\%b$ par définition, pour lequel il y a $k-1$ itérations. Par hypothèse de récurrence, on en déduit $b\\geq F_{k-1}$.\n",
    ">   Dans la deuxième, on a $a\\%b>0$ et on aboutit au couple $(a\\%b,b\\%(a\\%b))$ pour lequel il reste $k-2$ itérations. Par hypothèse de récurrence, on en déduit $a\\%b\\geq F_{k-2}$.\n",
    ">   Si on appelle $q$ le quotient dans la division eucilidienne de $a$ par $b$, on a par définition $a=bq+a\\%b$ et comme $a\\geq b$ on a $q\\geq 1$. Par conséquent on a $a\\geq b+a\\%b\\geq F_{k-1}+F_{k-2}=F_k$, c'est ce qu'on vouait démontrer.\n",
    ">\n",
    "> La propriété que l'on vient de démontrer peut se reformuler ainsi:\n",
    "> > En supposant $a\\geq b$, le nombre d'itérations de l'algorithme d'Euclide est majoré par le plus petit $k$ tel que $a\\leq F_k$.\n",
    ">\n",
    "> La formule de Binet permet d'avoir une idée de la façon dont $k$ croît en fonction de $a$: comme $\\phi>1$ et $|\\phi'|<1$, la suite $(F_n)$ est équivalente à $\\phi^n/\\sqrt{5}$. Si on note $k_a$ le plus petit $k$ tel que $a\\leq F_k$, on a donc que $a$ croît comme $\\phi^{k_a}/\\sqrt{5}$, autrement dit $k_a$ croît comme $\\log_\\phi(a\\sqrt{5})$.\n",
    "> Le nombre d'itérations est donc logarithmique en fonction de $a$.\n",
    ">\n",
    "> Dans le cas où $a<b$, la première itération aura pour effet d'échanger $a$ et $b$ (du fait de la division euclidienne $a=0\\times b+a$), le résultat s'appliquera ensuite au couple $(b,a)$. Ainsi, de façon générale, le nombre d'itérations est majoré par $\\log_\\phi(a)$ à une constante additive près.\n",
    "> On se passera d'une démonstration plus détaillée ici."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Calcul des coefficients de Bézout\n",
    "\n",
    "L'[identité de Bézout](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bachet-B%C3%A9zout) affirme:\n",
    "\n",
    "> **Théorème.** Pour tous entiers $a$ et $b$, il existe deux entiers $u$ et $v$ tels que $a∧b=au+bv$.\n",
    "\n",
    "La démonstration peut se faire en étendant le raisonnement utilisé pour le théorème d'Euclide:\n",
    "\n",
    "- Sans perte de généralité, on suppose $a≥b$.\n",
    "- On procède par récurrence (forte) sur $b$:\n",
    "  - Si $b=0$ alors $a∧b=a$ et donc $a∧b=1a+0b$.\n",
    "  - Si $b≠0$, on pose la division euclidienne $a=bq+r$, avec $0≤r<b$.  \n",
    "    Par hypothèse de récurrence, il existe $u'$ et $v'$ tel que $b∧r=u'b+v'r$.  \n",
    "    Comme $a∧b=b∧r$ et $r=a-bq$, on en déduit $a∧b=v'a+(u'-v'q)$.\n",
    "\n",
    "**Question 8.** Élaborer un algorithme itératif pour calculer les coefficients de Bézout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> On peut commencer par formuler un algorithme récursif, de la même façon que précédemment. La fonction va renvoyer un triplet $(d,u,v)$ où $d$ est le PGCD et $u$ et $v$ sont les coefficients de Bézout.\n",
    ">\n",
    "> ```python\n",
    "> def bezout_rec(a, b):\n",
    ">     if b == 0:\n",
    ">         d, u, v = a, 1, 0\n",
    ">     else:\n",
    ">         q = a // b\n",
    ">         (d, u, v) = bezout_rec(b, a % b)\n",
    ">         (u, v) = (v, u - v * q)\n",
    ">     return (d, u, v)\n",
    "> ```\n",
    ">\n",
    "> Contrairement au cas du PGCD vu au début, il ne s'agit plus d'une récursion terminale puisqu'on applique un traitement à la valeur renvoyée par l'appel récursif `bezout_rec(b,a%b)`. Il faut donc trouver un moyen de se souvenir des traitements à faire quand on passe à la version itérative.\n",
    ">\n",
    "> Pour ce faire, on peut remarquer que le traitement pour $u$ et $v$ consiste à faire des combinaisons linéaires des $u$ et $v$ obtenus par l'appel récursif, donc les valeurs finales de $u$ et $v$ seront des combinaisons linéaires des $u$ et $v$ du cas d'arrêt. Il suffit donc de retenir au cours de l'itération les coefficients à appliquer au résultat final. On peut définir cela par une fonction $B(a,b,x,y,z,t)$ telle que\n",
    "> $$ B(a,b,x,y,z,t)=(a\\wedge b,ux+vy,uz+vt) \\quad\\text{où}\\quad a\\wedge b=au+bv $$\n",
    "> de sorte que le résultat recherché est $B(a,b,1,0,0,1)$. Cette fonction, d'après les remarques précédentes, satisfait les équations suivantes:\n",
    " \\begin{align*}\n",
    "   B(a,0,x,y,z,t) &= (a, x, z) &&\\text{puisque } a\\wedge0 = a = a\\times1 + b\\times 0, \\\\\n",
    "   B(a,b,x,y,z,t) &= B(b, r, y, x-qy, t, z-qt) &&\\text{où } a=bq+r \\text{ et } 0\\leq r<b, \\text{si } b>0.\n",
    " \\end{align*}\n",
    "> puisque $u=v'$ et $v=u'-v'q$ impliquent $ux+vy=v'x+(u'-v'q)y=u'y+v'(x-qy)$ et de même $uz+vt=u't+v'(z-qt)$.\n",
    ">\n",
    "> Cette fonction donne l'algorithme suivant, qui est récursif terminal:\n",
    ">\n",
    "> ```python\n",
    "> def bezout_rec2(a, b, x, y, z, t):\n",
    ">     if b = 0:\n",
    ">         return (a, x, z)\n",
    ">     else:\n",
    ">         q = a // b\n",
    ">         return bezout_rec2(b, a % b, y, x - q * y, t, z - q * t)\n",
    "> ```\n",
    ">\n",
    "> Avec la même méthode qu'au dessus, on rend cet algorithme itératif de la façon suivante:\n",
    ">\n",
    "> ```python\n",
    "> def bezout_iter(a, b, x, y, z, t):\n",
    ">     while b != 0:\n",
    ">         q = a // b\n",
    ">         a, b = b, a % b\n",
    ">         x, y = y, x - q * y\n",
    ">         z, t = t, z - q * t\n",
    ">     return (a, x, z)\n",
    "> ```\n",
    ">\n",
    "> Tout ce qui reste à faire est d'initialiser les quatre variables $x,y,z,t$ à $1,0,0,1$ pour obtenir le résultat voulu."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 9.** Implémenter cet algorithme en Python, sous forme d'une fonction `bezout(a,b)` qui renvoit un triple `(d,u,v)` où `d` est le PGCD de `a` et `b` et où `u` et `v` sont les coefficients de Bézout associés."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def bezout(a, b):\n",
    "    x = t = 1\n",
    "    y = z = 0\n",
    "    while b != 0:\n",
    "        q = a // b\n",
    "        a, b = b, a % b\n",
    "        x, y = y, x - q * y\n",
    "        z, t = t, z - q * t\n",
    "    return a, x, z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le code qui suit est un programme qui teste cette fonction, en utilisant `math.gcd` pour vérifier que c'est bien le PGCD qui est calculé, et en vérifiant la relation de Bézout."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Résultats corrects pour 1000 essais.\n"
     ]
    }
   ],
   "source": [
    "essais = 1000\n",
    "ok = True\n",
    "for i in range(essais):\n",
    "    a = random.randint(0, 100000)\n",
    "    b = random.randint(0, 100000)\n",
    "    attendu = math.gcd(a, b)\n",
    "    obtenu, u, v = bezout(a, b)\n",
    "    if attendu != obtenu:\n",
    "        print(\"Résultat incorrect pour a=%d et b=%d: %d au lieu de %d.\" % (a, b, obtenu, attendu))\n",
    "        ok = False\n",
    "    if a * u + b * v != obtenu:\n",
    "        print(\"Relation non satisfaite: %d * %d + %d * %d != %d.\" % (a, u, b, v, obtenu))\n",
    "        ok = False\n",
    "if ok:\n",
    "    print(\"Résultats corrects pour %d essais.\" % essais)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Généralisation\n",
    "\n",
    "L'algorithme d'Euclide et sa version étendue qui donne les coefficients de Bézout s'applique aussi aux polynômes. On propose donc de faire en sorte que les fonctions précédentes s'appliquent à des polynômes.\n",
    "\n",
    "On représente des polynômes par des listes coefficients, l'élément de rang $i$ étant le coefficient de $X^i$. Par exemple $3X^3+5X-7$ sera représenté par la liste `[-7,0,5,3]`.\n",
    "\n",
    "- Programmer l'addition, la soustraction et la multiplication de polynômes.\n",
    "- Programmer la division euclidienne de polynômes.\n",
    "- Programmer le calcul du PGCD de polynômes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> On ne va pas donner une implémentation complète ici, c'est plutôt un thème pour prolonger la réflexion, sa réalisation complète demande du temps.\n",
    ">\n",
    "> - L'addition et la soustraction consistent à combiner deux listes en additionnant ou soustrayant les coefficients situés aux mêmes indices, en traitant correctement le cas de listes de longueurs différentes (cas de polynômes de degrés différents).\n",
    "> - La multiplication consiste à poser l'opération comme on le fait à la main: multiplier un polynôme par une constante consiste à multiplier chaque coefficient par cette constante; multiplier un polynôme par un monôme consiste à faire de même et en plus décaler les coefficients pour tenir compte du degré du monôme; multiplier un polynôme par un autre consiste à le multiplier par chacun des monômes et à additionner les résultats.\n",
    "> - Pour la division euclidienne, il s'agit encore de détailler l'opération faite à la main: tant que le dividende a un degré au moins égal à celui du diviseur, on lui soustrait le diviseur multiplié par le monôme qui est le quotient des monômes de plus hauts degrés, et on ajoute ce monôme au quotient. Quand le dividende est de degré inférieur au diviseur, c'est le reste de la division.\n",
    "> - Une fois ces quatre opérations programmées, l'algorithme d'Euclide s'écrit de la même façon que dans les entiers, en utilisant les opérations pour les polynômes."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
