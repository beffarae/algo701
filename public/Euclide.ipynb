{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Autour de l'algorithme d'Euclide\n",
    "\n",
    "Le but de cette séance est d'étudier le grand classique des algorithmes de l'arithmétique: [celui d'Euclide](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide), qui permet de calculer le PGCD de deux entiers. Autour de ce calcul, on va faire de la programmation en Python et de l'algorithmique.\n",
    "\n",
    "## 1. Le PGCD\n",
    "\n",
    "L'algorithme se base sur le théorème d'existence du PGCD:\n",
    "\n",
    "> **Théorème.**\n",
    "> Pour tous entiers $a$ et $b$, il existe un entier $d$ tel que\n",
    "> - $d$ divise $a$ et $b$,\n",
    "> - tout diviseur commun de $a$ et $b$ divise $d$.\n",
    "> \n",
    "> Cet entier est appelé PGCD (plus grand commun diviseur) de $a$ et $b$, on peut le noter $a∧b$.\n",
    "\n",
    "Grandes lignes de la démonstration:\n",
    "\n",
    "- Sans perte de généralité, on suppose $a≥b$.\n",
    "- On procède par récurrence (forte) sur $b$:\n",
    "  - Si $b=0$ alors $d=a$ convient.\n",
    "  - Si $b≠0$, on pose la division euclidienne $a=bq+r$, avec $0≤r<b$.  \n",
    "    Par hypothèse de récurrence, il existe $d$ qui divise $b$ et $r$ et tel que tout diviseur commun de $b$ et $r$ divise $d$.  \n",
    "    On vérifie que l'on a $d=a∧b$.\n",
    "\n",
    "## 2. Élaboration de l'algorithme\n",
    "\n",
    "La démonstration du théorème d'existence du PGCD donne un procédé de calcul.\n",
    "\n",
    "**Question 1.** En partant de la démonstration, expliciter les équations qui définissent $a∧b$ par récurrence en fonction de $a$ et $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Votre réponse ici…*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 2.** À partir de ces équations, définir une fonction Python récursive `pgcd_rec` qui calcule le PGCD de deux entiers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pgcd_rec(a, b):\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le code suivant teste que la fonction ainsi définie donne le bon résultat sur un série d'exemples pris au hasard, en utilisant la fonction `math.gcd` fournie par Python pour vérifier. Modifiez votre code jusqu'à ce que ça marche!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math, random\n",
    "def teste_pgcd(fonction, essais=1000):\n",
    "    ok = True\n",
    "    for i in range(essais):\n",
    "        a = random.randint(0, 100000)\n",
    "        b = random.randint(0, 100000)\n",
    "        attendu = math.gcd(a, b)\n",
    "        obtenu = fonction(a, b)\n",
    "        if attendu != obtenu:\n",
    "            print(\"Résultat incorrect pour a=%d et b=%d: %d au lieu de %d.\" % (a, b, obtenu, attendu))\n",
    "            ok = False\n",
    "    if ok:\n",
    "        print(\"Résultats corrects pour %d essais.\" % essais)\n",
    "teste_pgcd(pgcd_rec)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.** Démontrer que l'algorithme ainsi implémenté est correct, c'est-à-dire que la fonction renvoie toujours un résultat et que c'est toujours celui qu'il faut."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Votre réponse ici…*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'algorithme récursif que vous avez implémenté est un cas de [récursion terminale](https://fr.wikipedia.org/wiki/R%C3%A9cursion_terminale), c'est-à-dire que dans le cas où la fonction se rappelle elle-même récursivement, le résultat renvoyé est celui que renverra l'appel récursif. Un algorithme de cette forme peut être transformé pour être mis sous forme itérative, c'est-à-dire fondé sur une boucle.\n",
    "\n",
    "**Question 4.** Implémenter une version itérative de l'algorithme d'Euclide et justifier qu'elle est équivalente à la version récursive précédente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pgcd_iter(a, b):\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tester cette fonction pour vérifier qu'elle fonctionne:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "teste_pgcd(pgcd_iter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5.** Démontrer que l'algorithme itératif est correct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Votre réponse ici…*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Étude de complexité\n",
    "\n",
    "On rappelle la définition de la [suite de Fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci):\n",
    "$$\n",
    "    \\begin{align*}\n",
    "        F_0 &= 0 \\\\\n",
    "        F_1 &= 1 \\\\\n",
    "        F_n &= F_{n-1} + F_{n-2} \\qquad\\text{pour } n≥2\n",
    "    \\end{align*}\n",
    "$$\n",
    "La [formule de Binet](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci#Expression_fonctionnelle) en donne une expression close:\n",
    "$$\n",
    "    F_n = \\frac{1}{\\sqrt5}\\Bigl( φ^n - (φ')^n \\Bigr)\n",
    "    \\qquad\\text{où}\\quad φ = \\frac{1+\\sqrt5}{2}\n",
    "    \\quad\\text{et}\\quad φ' = \\frac{1-\\sqrt5}{2} .\n",
    "$$\n",
    "\n",
    "**Question 6.** Déterminer le nombre d'itérations que fait l'algorithme d'Euclide itératif si $a$ et $b$ sont deux termes consécutifs de la suite de Fibonacci."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Votre réponse ici…*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 7.** En déduire une majoration du nombre d'itérations utilisées pour le calcul de $a∧b$ en fonction de $a$ et $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Votre réponse ici…*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Calcul des coefficients de Bézout\n",
    "\n",
    "L'[identité de Bézout](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bachet-B%C3%A9zout) affirme:\n",
    "\n",
    "> **Théorème.** Pour tous entiers $a$ et $b$, il existe deux entiers $u$ et $v$ tels que $a∧b=au+bv$.\n",
    "\n",
    "La démonstration peut se faire en étendant le raisonnement utilisé pour le théorème d'Euclide:\n",
    "\n",
    "- Sans perte de généralité, on suppose $a≥b$.\n",
    "- On procède par récurrence (forte) sur $b$:\n",
    "  - Si $b=0$ alors $a∧b=a$ et donc $a∧b=1a+0b$.\n",
    "  - Si $b≠0$, on pose la division euclidienne $a=bq+r$, avec $0≤r<b$.  \n",
    "    Par hypothèse de récurrence, il existe $u'$ et $v'$ tel que $b∧r=u'b+v'r$.  \n",
    "    Comme $a∧b=b∧r$ et $r=a-bq$, on en déduit $a∧b=v'a+(u'-v'q)$.\n",
    "\n",
    "**Question 8.** Élaborer un algorithme itératif pour calculer les coefficients de Bézout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Votre réponse ici…*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 9.** Implémenter cet algorithme en Python, sous forme d'une fonction `bezout(a,b)` qui renvoit un triple `(d,u,v)` où `d` est le PGCD de `a` et `b` et où `u` et `v` sont les coefficients de Bézout associés."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def bezout(a, b):\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le code qui suit est un programme qui teste cette fonction, en utilisant `math.gcd` pour vérifier que c'est bien le PGCD qui est calculé, et en vérifiant la relation de Bézout."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "essais = 1000\n",
    "ok = True\n",
    "for i in range(essais):\n",
    "    a = random.randint(0, 100000)\n",
    "    b = random.randint(0, 100000)\n",
    "    attendu = math.gcd(a, b)\n",
    "    obtenu, u, v = bezout(a, b)\n",
    "    if attendu != obtenu:\n",
    "        print(\"Résultat incorrect pour a=%d et b=%d: %d au lieu de %d.\" % (a, b, obtenu, attendu))\n",
    "        ok = False\n",
    "    if a * u + b * v != obtenu:\n",
    "        print(\"Relation non satisfaite: %d * %d + %d * %d != %d.\" % (a, u, b, v, obtenu))\n",
    "        ok = False\n",
    "if ok:\n",
    "    print(\"Résultats corrects pour %d essais.\" % essais)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Généralisation\n",
    "\n",
    "L'algorithme d'Euclide et sa version étendue qui donne les coefficients de Bézout s'applique aussi aux polynômes. On propose donc de faire en sorte que les fonctions précédentes s'appliquent à des polynômes.\n",
    "\n",
    "On représente des polynômes par des listes coefficients, l'élément de rang $i$ étant le coefficient de $X^i$. Par exemple $3X^3+5X-7$ sera représenté par la liste `[-7,0,5,3]`.\n",
    "\n",
    "- Programmer l'addition, la soustraction et la multiplication de polynômes.\n",
    "- Programmer la division euclidienne de polynômes.\n",
    "- Programmer le calcul du PGCD de polynômes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
